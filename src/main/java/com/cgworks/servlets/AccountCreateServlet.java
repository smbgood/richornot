package com.cgworks.servlets;

import com.cgworks.data.RONAccount;
import com.cgworks.data.RONPrincipal;
import com.cgworks.services.EmailMessagingService;
import com.cgworks.services.RONAccountManager;
import com.cgworks.services.UserNameFilterService;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.query.Query;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Iterator;

@SlingServlet(paths="/bin/account/create", methods="POST")
public class AccountCreateServlet extends SlingAllMethodsServlet {

    @Reference
    private SlingRepository repo;

    @Reference
    private ResourceResolverFactory rrf;

    @Reference
    private RONAccountManager accountManager;

    @Reference
    private UserNameFilterService filterService;

    @Reference
    private EmailMessagingService emailService;

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){
        String errorMessage = "";

        String username = RONUtils.safeGetRequestParam("username", request);

        boolean userNameOK = RONUtils.checkUserNameFilter(username, filterService);
        if(!userNameOK){
            RONUtils.outputErrorJson("swears", response);
            return;
        }

        String password = RONUtils.safeGetRequestParam("password", request);
        String email = RONUtils.safeGetRequestParam("email", request);

        Session session = null;
        try {
            session = repo.login(new SimpleCredentials("admin", "admin".toCharArray()));
        } catch (RepositoryException e) {
            errorMessage = e.getMessage();
        }

        ResourceResolver resolver = RONUtils.loginWithAdminCredentials(session, rrf);
        if(resolver != null) {

            //do not allow account creation if username already exists / email already exists
            Iterator<Resource> foundResources = resolver.findResources("SELECT * FROM [rep:User] AS s WHERE ISDESCENDANTNODE(s,'/home/users') AND s.[email]='" +email + "'", Query.JCR_SQL2);
            if(foundResources.hasNext()){
                errorMessage = "email in use";
                RONUtils.outputErrorJson(errorMessage, response);
                if(session.isLive())
                    session.logout();
                return;
            }
        }



        if(session instanceof JackrabbitSession)
        {
            try {
                UserManager um = ((JackrabbitSession) session).getUserManager();
                if(um != null){
                    RONPrincipal account = new RONPrincipal(email, "");
                    User user = um.createUser(username, password, account, null);
                    if(user != null){
                        RONAccount ronAccount = new RONAccount(user, email);
                        if(accountManager != null){
                            accountManager.addAccount(account.getSeedHex(), ronAccount);
                        }

                        if(emailService != null){
                            emailService.sendMessage(Constants.EMAIL_SIGNUP, "", "", Constants.ADMIN_FROM_EMAIL, email);
                        }

                        Resource userResource = resolver.getResource(user.getPath());
                        if(userResource != null){
                            ModifiableValueMap mvm = userResource.adaptTo(ModifiableValueMap.class);
                            if(mvm != null){
                                mvm.put("email", email);
                                mvm.put("seed", account.getSeed());
                            }
                        }
                        session.save();
                        try {
                            response.setCharacterEncoding("UTF-8");
                            response.setContentType("application/json");
                            response.getWriter().write("{\"token\":\"" + account.getName() + "\"}");
                        } catch (IOException e) {
                            errorMessage = e.getMessage();
                        }
                    }
                }
            } catch (RepositoryException e) {
                errorMessage = e.getMessage();
            } catch (MessagingException e) {
                errorMessage = e.getMessage();
            }
            RONUtils.outputErrorJson(errorMessage, response);
            if(session.isLive())
                session.logout();
            //request is async posted so no response code here as it would be ignored
        }
    }
}
