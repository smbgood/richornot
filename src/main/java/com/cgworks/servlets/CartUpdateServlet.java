package com.cgworks.servlets;

import com.cgworks.data.RONCart;
import com.cgworks.services.UserCartService;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import java.io.IOException;

@SlingServlet(paths="/bin/addToCart", methods="POST")
public class CartUpdateServlet extends SlingAllMethodsServlet{

    @Reference
    private UserCartService cartService;

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){
        try {
            String productPath = request.getRequestParameter("resource") != null ? request.getRequestParameter("resource").getString() : "";
            int quantity = request.getRequestParameter("quantity") != null ? Integer.parseInt(request.getRequestParameter("quantity").getString()) : 0;
            String userToken = RONUtils.deObfuscateToken(RONUtils.safeGetCookie(Constants.USER_COOKIE_TOKEN_STRING, request));

            String tokenToUse = "";
            String guestToken = RONUtils.safeGetAttribute(Constants.GUEST_COOKIE_TOKEN_STRING, request);

            if(StringUtils.isBlank(userToken)){
                tokenToUse = guestToken;
            }else{
                tokenToUse = userToken;
            }

            boolean isGuest = tokenToUse.equals(guestToken);

            if(quantity > 0 && StringUtils.isNotBlank(productPath) && StringUtils.isNotBlank(tokenToUse)){
                RONCart cart = RONUtils.getOrCreateCartAndUpdate(cartService, request.getResourceResolver(), productPath, quantity, tokenToUse, isGuest);
                if(cart != null){
                    //success
                    response.sendRedirect(productPath + ".html");
                    return;
                }
            }

            response.sendRedirect(Constants.CONTENT_ROOT + "/home.html");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}