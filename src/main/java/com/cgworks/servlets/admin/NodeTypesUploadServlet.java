package com.cgworks.servlets.admin;

// class to read in uploaded .cnd file and update node types with the results of the file

import com.cgworks.utils.RONUtils;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.api.JackrabbitNodeTypeManager;
import org.apache.jackrabbit.commons.cnd.CndImporter;
import org.apache.jackrabbit.commons.cnd.ParseException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;
import java.io.IOException;
import java.io.InputStreamReader;

@SlingServlet(paths="/bin/admin/nodetypes", methods = "POST")
public class NodeTypesUploadServlet extends SlingAllMethodsServlet{

    @Reference
    SlingRepository repo;

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        //expects a .cnd file
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if(isMultipart) {
            RequestParameter p = request.getRequestParameter("cndUpload");
            if (p != null && p.getContentType() != null) {
                try {
                    NodeType[] nodeTypes = CndImporter.registerNodeTypes(new InputStreamReader(p.getInputStream()), RONUtils.loginSessionWithAdmin(repo));
                    for (NodeType nt : nodeTypes) {
                        System.out.println("Registered: " + nt.getName());
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (RepositoryException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            response.sendRedirect(request.getHeader("Referer"));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
