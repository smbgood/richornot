package com.cgworks.servlets.admin;

import com.cgworks.services.SiteNavService;
import com.cgworks.services.admin.ResourceTypeService;
import com.cgworks.utils.RONUtils;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.util.Text;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@SlingServlet(paths="/bin/admin/page/manage", methods = "POST")
public class PageManageServlet extends SlingAllMethodsServlet {

    @Reference
    private SlingRepository repo;

    @Reference
    private ResourceResolverFactory rrf;

    @Reference
    private
    SiteNavService navService;

    @Reference
    private
    ResourceTypeService resourceTypeService;

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){

        RequestParameterMap parameterMap = request.getRequestParameterMap();

        ArrayList<String> pathsToCreate = getStringsForIndex("create", parameterMap);

        ArrayList<String> authPaths = getStringsForIndex("makeAuth", parameterMap);

        String action = RONUtils.safeGetRequestParamFromMap("action", parameterMap);

        String resourceType = RONUtils.safeGetRequestParamFromMap("resourceType", parameterMap);

        String title = RONUtils.safeGetRequestParamFromMap("pageTitle", parameterMap);

        String nodeName = RONUtils.safeGetRequestParamFromMap("nodeName", parameterMap);

        ArrayList<SimplePage> pagesToCreate = getPagesToCreate(pathsToCreate, "page", parameterMap, resourceType, title, nodeName);

        if (CollectionUtils.isNotEmpty(authPaths))
            pagesToCreate.addAll(getPagesToCreate(authPaths, "auth", parameterMap, resourceType, title, nodeName));

        if (CollectionUtils.isNotEmpty(pagesToCreate)) {
            ResourceResolver resolver = null;
            try {
                resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
                if (resolver != null) {
                    for (SimplePage p : pagesToCreate) {
                        if(StringUtils.equals(action, "Create")) {
                            Map<String, Object> props = new HashMap<String, Object>();
                            props.put("sling:resourceType", p.resourceType);
                            props.put("title", p.title);
                            Map<String, Object> templateProps = resourceTypeService.getPagePropsForRType(p.resourceType);
                            props.putAll(templateProps);
                            ResourceUtil.getOrCreateResource(resolver, p.path + "/" + Text.escapeIllegalJcrChars(p.nodeName), props, null, true);
                        }else if(StringUtils.equals(action, "Delete")) {
                            Resource resource = resolver.getResource(p.path);
                            if(resource != null)
                                resolver.delete(resource);
                        }
                    }
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            } catch (PersistenceException e) {
                e.printStackTrace();
            }

            if(resolver != null) {
                try {
                    resolver.commit();
                } catch (PersistenceException e) {
                    e.printStackTrace();
                }
                resolver.close();
            }

            if (navService != null)
                navService.rebuildSiteNavigationTree();
        }

        try {
            response.sendRedirect(request.getHeader("Referer"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<String> getStringsForIndex(String index, RequestParameterMap parameterMap){
        ArrayList<String> pathsToCreate = new ArrayList<String>();
        for(String entry : parameterMap.keySet()){
            if(entry.contains(index)){
                pathsToCreate.add(entry);
            }
        }
        return pathsToCreate;
    }

    private ArrayList<SimplePage> getPagesToCreate(ArrayList<String> pathsToCreate, String propertyName, RequestParameterMap parameterMap, String resourceType, String title, String nodeName){
        ArrayList<SimplePage> pagesToCreate = new ArrayList<SimplePage>();
        for(String s : pathsToCreate){
            String arrayIndex = s.substring(s.indexOf("["), s.lastIndexOf("]") + 1);
            final String path = parameterMap.get(propertyName + arrayIndex)[0].getString();
            pagesToCreate.add(new SimplePage(resourceType, title, path, nodeName));
        }
        return pagesToCreate;
    }

    private class SimplePage {
        public String title;

        String resourceType;

        public String path;

        public String nodeName;

        SimplePage(String resourceType, String title, String path, String nodeName) {
            this.resourceType = resourceType;
            this.title = title;
            this.path = path;
            this.nodeName = nodeName;
        }
    }
}
