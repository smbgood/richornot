package com.cgworks.servlets.admin;

import com.cgworks.services.OrderManager;
import com.cgworks.utils.RONUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import java.io.IOException;
import java.util.ArrayList;

@SlingServlet(paths="/bin/admin/order/manage", methods = "POST")
public class OrderManageServlet extends SlingAllMethodsServlet {

    @Reference
    private SlingRepository repo;

    @Reference
    private ResourceResolverFactory rrf;

    @Reference
    private OrderManager orderManager;

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){
        RequestParameterMap parameterMap = request.getRequestParameterMap();
        ArrayList<String> usersToDelete = new ArrayList<String>();
        for(String entry : parameterMap.keySet()){
            if(entry.contains("delete")){
                usersToDelete.add(entry);
            }
        }

        ArrayList<String> pathsToDelete = new ArrayList<String>();
        for(String s : usersToDelete){
            String indexToDelete = s.substring(s.indexOf("[") + 1, s.indexOf("]", s.indexOf("[")));
            final String path = parameterMap.get("order[" + indexToDelete + "][path]")[0].getString();
            pathsToDelete.add(path);
        }

        ResourceResolver resolver = null;
        try {
            resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        ArrayList<String> idsToRemove = new ArrayList<String>();

        if(resolver != null) {
            for (String path : pathsToDelete) {
                Resource orderResource = resolver.getResource(path);
                if (orderResource != null) {
                    idsToRemove.add(orderResource.getName());
                    try {
                        resolver.delete(orderResource);
                    } catch (PersistenceException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        if(CollectionUtils.isNotEmpty(idsToRemove)){
            for(String id : idsToRemove){
                orderManager.deleteOrder(id);
            }
        }

        try{
            response.sendRedirect(request.getHeader("Referer"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(resolver != null){
            try {
                resolver.commit();
            } catch (PersistenceException e) {
                e.printStackTrace();
            }
        }

    }
}
