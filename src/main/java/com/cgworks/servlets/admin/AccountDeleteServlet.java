package com.cgworks.servlets.admin;

import com.cgworks.data.RONAccount;
import com.cgworks.services.RONAccountManager;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import java.io.IOException;
import java.util.ArrayList;

@SlingServlet(paths="/bin/account/delete", methods = "POST")
public class AccountDeleteServlet extends SlingAllMethodsServlet {

    @Reference
    private RONAccountManager accountManager;

    @Reference
    private SlingRepository repo;

    @Reference
    private ResourceResolverFactory rrf;

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){
        RequestParameterMap parameterMap = request.getRequestParameterMap();
        ArrayList<String> usersToDelete = new ArrayList<String>();
        for(String entry : parameterMap.keySet()){
            if(entry.contains("delete")){
                usersToDelete.add(entry);
            }
        }

        ArrayList<SimpleAccount> accountsToDelete = new ArrayList<SimpleAccount>();
        for(String s : usersToDelete){
            String indexToDelete = s.substring(s.indexOf("[") + 1, s.lastIndexOf("]"));
            final String path = parameterMap.get("users[" + indexToDelete + "][path]")[0].getString();
            final String token = parameterMap.get("users[" + indexToDelete + "][token]")[0].getString();
            accountsToDelete.add(new SimpleAccount(token, path));
        }

        ResourceResolver resolver = null;
        try {
            resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

        for(SimpleAccount account : accountsToDelete){
            String userPath = account.path;
            if(StringUtils.isNotBlank(userPath)){
                if(resolver != null){
                    try {
                        final Resource resource = resolver.getResource(userPath);
                        if(resource != null) {
                            resolver.delete(resource);
                        }
                    } catch (PersistenceException e) {
                        e.printStackTrace();
                    }
                }
            }
            String userToken = account.token;
            String deObfuscated = RONUtils.deObfuscateToken(userToken);
            if(StringUtils.isNotBlank(deObfuscated)){
                accountManager.removeAccount(deObfuscated);
            }
        }

        try {
            response.sendRedirect(request.getHeader("Referer"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(resolver != null){
            try {
                resolver.commit();
            } catch (PersistenceException e) {
                e.printStackTrace();
            }
        }
    }

    private class SimpleAccount{
        public String token;
        public String path;

        public SimpleAccount(String token, String path) {
            this.token = token;
            this.path = path;
        }
    }
}
