package com.cgworks.servlets.admin;

import com.cgworks.utils.RONUtils;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;

import javax.imageio.ImageIO;
import javax.jcr.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


@SlingServlet(paths = "/bin/admin/fileupload", methods="POST", resourceTypes = {"sling/servlet/default" }, extensions = ".html")
public class FileUploadServlet extends SlingAllMethodsServlet {

    @Reference
    ResourceResolverFactory rrf;

    @Reference
    SlingRepository repo;

    private Map<String, Object> resourceProps = new HashMap<String, Object>(){{
        put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_RESOURCE);
    }};

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        if(isMultipart) {
            RequestParameter p = request.getRequestParameter("fileUpload");
            if(p != null && p.getContentType() != null && p.getContentType().contains("image")){
                String extension = p.getContentType().replace("image/", "");
                try {
                    Session s = RONUtils.loginSessionWithAdmin(repo);
                    ResourceResolver resourceResolver = RONUtils.loginWithAdminCredentials(s, rrf);
                    if(resourceResolver != null){
                        Resource root = resourceResolver.getResource("/uploads");
                        if(root != null) {
                            Map<String, Object> fileProps = new HashMap<String, Object>();
                            fileProps.put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_FILE);
                            Resource named = resourceResolver.create(root, p.getFileName(), new HashMap<String, Object>(){{put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED);}});
                            if(named != null){
                                InputStream inputStream = p.getInputStream();
                                BufferedImage image = ImageIO.read(inputStream);
                                ValueFactory factory = s.getValueFactory();

                                Binary binary = convertImageToBinary(image, factory, extension);

                                Resource rendition = resourceResolver.create(named, "renditions", new HashMap<String, Object>(){{put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED);}});
                                if(image != null) {
                                    int inWidth = image.getWidth();
                                    int inHeight = image.getHeight();

                                    resourceProps.put(JcrConstants.JCR_MIMETYPE, p.getContentType());
                                    String originalExtension = p.getFileName().substring(p.getFileName().indexOf("."));

                                    String originalFileName = p.getFileName().replace(originalExtension, "") + "_original_" + String.valueOf(image.getWidth()) + "x" + String.valueOf(image.getHeight()) + originalExtension;
                                    Resource createdParent = resourceResolver.create(rendition, originalFileName, fileProps);
                                    Resource created = resourceResolver.create(createdParent, JcrConstants.JCR_CONTENT, resourceProps);
                                    addBinaryData(binary, created, s, resourceResolver);
                                    Node contentNode = s.getNode(created.getPath());
                                    if(contentNode != null){
                                        contentNode.addMixin("ron:imageMetadata");
                                        contentNode.setProperty("ron:width", String.valueOf(image.getWidth()));
                                        contentNode.setProperty("ron:height", String.valueOf(image.getHeight()));
                                    }
                                    //measure image

                                    final PhotoSize superSmallSize = new PhotoSize(50, 50);
                                    final PhotoSize xtraSmallSize = new PhotoSize(100,100);
                                    final PhotoSize smallSize = new PhotoSize(200, 200);
                                    final PhotoSize medSize = new PhotoSize(400, 400);

                                    ArrayList<PhotoSize> sizes = new ArrayList<PhotoSize>(){{add(superSmallSize); add(xtraSmallSize); add(smallSize); add(medSize);}};

                                    for(PhotoSize size : sizes) {

                                        //if exceeds size, calc factor of x and y that we need to shift

                                        double scaleX = (size.getWidth() + 0.0d) / inWidth;
                                        double scaleY = (size.getHeight() + 0.0d) / inHeight;

                                        BufferedImage scaled = RONUtils.scale(image, image.getType(), size.getWidth(), size.getHeight(), scaleX, scaleY);
                                        String fileName = p.getFileName().replace(originalExtension, "") + "_" + String.valueOf(size.getWidth()) + "x" + String.valueOf(size.getHeight()) + originalExtension;
                                        Resource scaledImageParent = resourceResolver.create(rendition, fileName, fileProps);
                                        Binary scaledBinary = convertImageToBinary(scaled, factory, extension);

                                        Resource scaledImage = resourceResolver.create(scaledImageParent, JcrConstants.JCR_CONTENT, resourceProps);
                                        addBinaryData(scaledBinary, scaledImage, s, resourceResolver);
                                        Node scaledNode = s.getNode(scaledImage.getPath());
                                        if (scaledNode != null) {
                                            scaledNode.addMixin("ron:imageMetadata");
                                            scaledNode.setProperty("ron:width", String.valueOf(scaled.getWidth()));
                                            scaledNode.setProperty("ron:height", String.valueOf(scaled.getHeight()));
                                        }
                                        s.save();
                                    }
                                }
                            }
                        }
                        resourceResolver.close();
                        s.logout();
                    }
                } catch (RepositoryException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                response.sendRedirect(request.getHeader("Referer"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Binary convertImageToBinary(BufferedImage image, ValueFactory factory, String extension) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, extension, baos);
            baos.flush();
            byte[] imageInByte = baos.toByteArray();
            baos.close();
            InputStream iss = new ByteArrayInputStream(imageInByte);
            Binary b = factory.createBinary(iss);
            iss.close();
            return b;
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private void addBinaryData(Binary binary, Resource content, Session session, ResourceResolver resourceResolver) {
        if(content != null && session != null && binary != null){
            try {
                Node contentNode = session.getNode(content.getPath());
                if(contentNode != null){
                    contentNode.setProperty(JcrConstants.JCR_DATA, binary);
                }
                session.save();
                resourceResolver.commit();
            } catch (RepositoryException e) {
                e.printStackTrace();
            } catch (PersistenceException e) {
                e.printStackTrace();
            }
        }
    }

    private class PhotoSize {
        private final int height;

        public int getHeight() {
            return height;
        }

        public int getWidth() {
            return width;
        }

        private final int width;

        public PhotoSize(int width, int height) {
            this.width = width;
            this.height = height;
        }
    }
}
