package com.cgworks.servlets.test;

import com.cgworks.slingmodels.RONComment;
import com.cgworks.services.CommentService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

@SlingServlet(paths = "/bin/comments", methods="GET")
public class CommentTestServlet extends SlingAllMethodsServlet {

    @Reference
    private
    CommentService commentService;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        try {
            PrintWriter p = response.getWriter();
            if(commentService != null){
                p.write(commentService.getPathMappings().size());
            }else{
                p.write("no comments");
            }
            p.flush();
            p.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
