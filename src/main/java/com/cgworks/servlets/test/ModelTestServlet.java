package com.cgworks.servlets.test;

import com.cgworks.slingmodels.RONComment;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import java.io.IOException;
import java.io.PrintWriter;

@SlingServlet(paths = "/bin/model", methods="GET")
public class ModelTestServlet extends SlingAllMethodsServlet {

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        try {
            PrintWriter pw = response.getWriter();
        ResourceResolver rr = request.getResourceResolver();
        Resource test = rr.getResource("/etc/00");
        if(test != null){
            RONComment testy = test.adaptTo(RONComment.class);
            if(testy != null && StringUtils.isNotBlank(testy.getSomeMessage())){
                pw.write(testy.getSomeMessage());
            }else{
                pw.write("nope");
            }
        }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
