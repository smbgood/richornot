package com.cgworks.servlets.test;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import java.io.IOException;
import java.util.Enumeration;

@SlingServlet(paths = "/bin/testy", methods = "POST")
public class PostTestServlet extends SlingAllMethodsServlet {

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){
        try {
            Enumeration<String> paramNames = request.getParameterNames();
            while(paramNames.hasMoreElements()){
                String paramName = paramNames.nextElement();
                String[] paramValues = request.getParameterValues(paramName);
                if(paramValues.length == 1){
                    response.getWriter().write(paramName + "   " + paramValues[0]);
                }else{
                    for(String s : paramValues){
                        response.getWriter().write(paramName + "   " + s);
                    }
                }
            }
            response.getWriter().write("we posted");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
