package com.cgworks.servlets;

import com.cgworks.data.RONAccount;
import com.cgworks.data.RONPrincipal;
import com.cgworks.services.RONAccountManager;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import java.io.IOException;
import java.security.Principal;

@SlingServlet(paths = "/bin/account/login", methods="POST")
public class AccountLoginServlet extends SlingAllMethodsServlet {

    @Reference
    private SlingRepository repo;

    @Reference
    private ResourceResolverFactory rrf;

    @Reference
    private RONAccountManager accountManager;

    //passed a login token, get user account from service
    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){

        String username = RONUtils.safeGetRequestParam("username", request);
        String password = RONUtils.safeGetRequestParam("password", request);

        String errorReason = "";

        Session session = null;
        try {
            session = repo.login(new SimpleCredentials(username, password.toCharArray()));
        } catch (RepositoryException e) {
            errorReason = e.getMessage();
        }

        if(session != null && session.isLive() && session instanceof JackrabbitSession){
            try {
                JackrabbitSession u = (JackrabbitSession) session;
                final UserManager userManager = u.getUserManager();
                if(userManager != null) {
                    final Authorizable authorizable = userManager.getAuthorizable(username);
                    if (authorizable != null) {
                        String userPath = authorizable.getPath();
                        RONAccount account = accountManager.getAccountFromPath(userPath);
                        if(account != null){
                            response.setCharacterEncoding("UTF-8");
                            response.setContentType("application/json");
                            try {
                                response.getWriter().write("{\"token\":\"" + account.getUserPrincipal().getName() + "\"}");
                            } catch (IOException e) {
                                errorReason = e.getMessage();
                            }
                        }
                    }
                }


            } catch (RepositoryException e) {
                errorReason = e.getMessage();
            }
        }
        RONUtils.outputErrorJson(errorReason, response);

    }


}
