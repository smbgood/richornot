package com.cgworks.servlets;

import com.cgworks.data.OrderStatus;
import com.cgworks.data.RONAccount;
import com.cgworks.data.RONCart;
import com.cgworks.data.RONItem;
import com.cgworks.services.EmailMessagingService;
import com.cgworks.services.OrderManager;
import com.cgworks.services.RONAccountManager;
import com.cgworks.services.UserCartService;
import com.cgworks.slingmodels.RONOrder;
import com.cgworks.slingmodels.RONPage;
import com.cgworks.slingmodels.RONProduct;
import com.cgworks.slingmodels.RONUser;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import com.google.gson.Gson;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

//this in a perfect world will process credit card transactions
@SlingServlet(paths={"/bin/cart/create","/bin/cart/create-reviewed"}, methods="POST")
public class OrderCreateServlet extends SlingAllMethodsServlet{

    @Reference
    private RONAccountManager accountManager;

    @Reference
    private UserCartService cartService;

    @Reference
    private OrderManager orderManager;

    @Reference
    private SlingRepository repo;

    @Reference
    private ResourceResolverFactory rrf;

    @Reference
    private EmailMessagingService emailService;

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        ResourceResolver resolver = null;
        try {
            resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
            Resource created = null;
            String userToken = "";
            String json = RONUtils.safeGetRequestParam("orderJson", request);
            if (StringUtils.isNotBlank(json)) {
                RONCart cart = new Gson().fromJson(json, RONCart.class);
                if (cart != null) {
                    final String guestToken = RONUtils.safeGetAttribute(Constants.GUEST_COOKIE_TOKEN_STRING, request);
                    String tokenToUse = cart.isGuest() ? guestToken : userToken;
                    if (StringUtils.isBlank(tokenToUse)) {
                        tokenToUse = cart.getTokenId();
                    }
                    if (cartService != null && StringUtils.isNotBlank(tokenToUse)) {
                        cartService.removeCart(tokenToUse);
                    }

                    //check quantity of items here before order create
                    // if 1 or more have gone out of stock since they added to cart
                    // need to mark those items as out of stock in cart
                    // and re-fill in entered details (low priority)

                    //reviewed orders should have their partial and fulls checked again and if necessary rererouted until they can order
                    boolean isReview = request.getRequestPathInfo().getResourcePath().contains("review");

                    ArrayList<RONItem> fullOrders = new ArrayList<RONItem>();
                    ArrayList<RONItem> partialOrders = new ArrayList<RONItem>();
                    ArrayList<RONItem> nullOrders = new ArrayList<RONItem>();

                    if(isReview){
                        for(RONItem item : cart.getOrderableItems()){
                            resolveQuantities(item, fullOrders, partialOrders, nullOrders, resolver);
                        }
                        for(RONItem partialItem : cart.getPartialItems()){
                            resolveQuantities(partialItem, fullOrders, partialOrders, nullOrders, resolver);
                        }
                    }else {
                        //list can order
                        for (RONItem item : cart.getItems().values()) {
                            resolveQuantities(item, fullOrders, partialOrders, nullOrders, resolver);
                        }
                    }

                    cart.setOrderableItems(fullOrders);
                    cart.setPartialItems(partialOrders);
                    cart.setNullItems(nullOrders);

                    //no null or partials, go ahead and create the order
                    if(CollectionUtils.isEmpty(nullOrders) && CollectionUtils.isEmpty(partialOrders)) {
                        created = handleOrderCreation(cart, request, resolver, accountManager);
                    }else{
                        cartService.updateCart(tokenToUse, cart);
                        //go to 'check order' page
                        RONPage reviewPage = RONUtils.findPageByResourceType(Constants.REVIEW_RTYPE, request);
                        if(reviewPage != null && StringUtils.isNotBlank(reviewPage.getResourcePath())){
                            response.sendRedirect(reviewPage.getResourcePath() + ".html");
                        }else{
                            response.sendRedirect(Constants.CONTENT_ROOT + "/error.html");
                        }
                        return;
                    }
                }
                if (created != null) {
                    RONOrder order = created.adaptTo(RONOrder.class);
                    if (order != null && orderManager != null) {
                        orderManager.addOrder(order, created.getName());

                        if(emailService != null){
                            String email = "";
                            if(StringUtils.isNotBlank(order.getGuestEmail())){
                                email = order.getGuestEmail();
                            }else if(StringUtils.isNotBlank(userToken)){
                                RONAccount currentUser = accountManager.getAccountFromToken(userToken);
                                email = currentUser.getUserEmail();
                            }
                            try {
                                emailService.sendMessage(Constants.EMAIL_ORDER_CREATE, "", json, Constants.ADMIN_FROM_EMAIL, email);
                            }catch(Exception e){
                                System.out.println(e.getMessage());
                            }
                        }
                    }

                    RONPage checkoutPage = RONUtils.findPageByResourceType(Constants.CHECKOUT_RTYPE, request);
                    if (checkoutPage != null && StringUtils.isNotBlank(checkoutPage.getResourcePath()))
                        response.sendRedirect(checkoutPage.getResourcePath() + ".html?order=" + created.getName());
                    else
                        response.sendRedirect(Constants.CONTENT_ROOT + "/error.html");
                } else {
                    //failure
                    response.sendRedirect(Constants.CONTENT_ROOT + "/error.html");
                }
            }

        } catch (IOException | RepositoryException e) {
            e.printStackTrace();
        }

        if (resolver != null && resolver.isLive())
            resolver.close();
    }

    private Resource handleOrderCreation(RONCart cart, SlingHttpServletRequest request, ResourceResolver resolver, RONAccountManager accountManager) throws PersistenceException, RepositoryException {
        if (cart.isGuest()) {
            String name = RONUtils.safeGetRequestParam("name", request);
            String email = RONUtils.safeGetRequestParam("email", request);
            String address = RONUtils.safeGetRequestParam("address", request);
            Map<String, Object> props = new HashMap<String, Object>();
            props.put("guestName", name);
            props.put("guestEmail", email);
            props.put("guestAddress", address);
            props.put("isGuestOrder", true);
            return createOrderJcrInfo(cart, true, null, props, resolver);
        } else {
            String userToken = RONUtils.deObfuscateToken(RONUtils.safeGetCookie(Constants.USER_COOKIE_TOKEN_STRING, request));
            if (StringUtils.isNotBlank(userToken)) {
                if (accountManager != null) {
                    RONAccount account = accountManager.getAccountFromToken(userToken);
                    if (account != null) {
                        return createOrderJcrInfo(cart, false, account, null, resolver);
                    }
                }
            }
        }
        return null;
    }

    private void resolveQuantities(RONItem item, ArrayList<RONItem> fullOrders, ArrayList<RONItem> partialOrders, ArrayList<RONItem> nullOrders, ResourceResolver resolver) {
        int orderableAmount = RONUtils.checkQuantities(item, resolver);
        if (orderableAmount == item.getQuantity()) {
            fullOrders.add(item);
        } else if (orderableAmount > 0) {
            item.setQuantity(orderableAmount);
            partialOrders.add(item);
        } else {
            nullOrders.add(item);
        }
    }

    private Resource createOrderJcrInfo(RONCart cart, boolean isGuest, RONAccount account, Map<String, Object> guestProps, ResourceResolver resolver) throws RepositoryException, PersistenceException {
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("jcr:primaryType", "nt:unstructured");
        Resource orderStorage = ResourceUtil.getOrCreateResource(resolver, Constants.CONTENT_ROOT + "/orders", props, null, true);
        Map<String, Object> childProps = new HashMap<String, Object>();
        childProps.put("jcr:primaryType", "nt:unstructured");
        childProps.put("sling:resourceType", "components/order-detail");
        childProps.put("orderStatus", OrderStatus.CREATED);
        childProps.put("createdDate", System.currentTimeMillis());
        childProps.put("orderTotal", cart.getGrandTotal());
        //add full and partials to items ordered, update quantities
        for(RONItem item : cart.getOrderableItems()){
            updateQuantities(resolver, item);
        }
        for(RONItem item : cart.getPartialItems()){
            updateQuantities(resolver, item);
        }
        //partial and null items get reported back
        childProps.put("orderJson", cart.getOrderJson());
        if(isGuest){
            childProps.putAll(guestProps);
        }else{
            childProps.put("userOrdered", account.getUserPath());
        }
        String uniqueNodeName = UUID.randomUUID() + "";
        while(orderStorage.getChild(uniqueNodeName) != null){
            uniqueNodeName = UUID.randomUUID() + "";
        }
        Resource created = resolver.create(orderStorage, uniqueNodeName, childProps);

        resolver.commit();

        return created;
    }

    private void updateQuantities(ResourceResolver resolver, RONItem item) {
        if(item == null || item.getProductPath() == null)
            return;
        Resource resource = resolver.getResource(item.getProductPath());
        if(resource == null)
            return;
        ModifiableValueMap mvm = resource.adaptTo(ModifiableValueMap.class);
        if(mvm != null){
            int newQuantity = mvm.get("quantity", 0) - item.getQuantity();
            mvm.put("quantity", newQuantity);
        }
    }

}
