package com.cgworks.servlets;


import com.cgworks.services.UserCartService;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import java.io.IOException;

@SlingServlet(paths="/bin/cart/clear", methods="POST")
public class CartClearServlet extends SlingAllMethodsServlet{

    @Reference
    private UserCartService cartService;

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){
        String tokenToUse = "";
        String token = RONUtils.deObfuscateToken(RONUtils.safeGetCookie(Constants.USER_COOKIE_TOKEN_STRING, request));
        String guestToken = RONUtils.safeGetAttribute(Constants.GUEST_COOKIE_TOKEN_STRING, request);
        tokenToUse = StringUtils.isNotBlank(token) ? token : guestToken;
        if(StringUtils.isNotBlank(tokenToUse)){
            if(cartService != null){
                cartService.removeCart(tokenToUse);
            }
        }
        String redirectPath = request.getRequestParameter("redirect") != null ? request.getRequestParameter("redirect").getString() : Constants.DEFAULT_REDIRECT_HOME;
        try {
            response.sendRedirect(redirectPath + ".html");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
