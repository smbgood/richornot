package com.cgworks.servlets;

import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;


@SlingServlet(paths="/bin/account/logout", methods="POST")
public class AccountLogoutServlet extends SlingAllMethodsServlet {

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){

        String userCookie = RONUtils.deObfuscateToken(RONUtils.safeGetCookie(Constants.USER_COOKIE_TOKEN_STRING, request));
        if(StringUtils.isNotBlank(userCookie)){
            //set logout time ? do other logout tasks
        }

        response.setStatus(200);

    }

}
