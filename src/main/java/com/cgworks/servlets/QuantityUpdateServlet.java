package com.cgworks.servlets;

import com.cgworks.data.RONItem;
import com.cgworks.slingmodels.RONProduct;
import com.cgworks.utils.RONUtils;
import com.google.gson.JsonObject;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;

@SlingServlet(paths = "/bin/cart/quantity", methods = "POST")
public class QuantityUpdateServlet extends SlingAllMethodsServlet {

    @Reference
    private SlingRepository repo;

    @Reference
    private ResourceResolverFactory rrf;

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){
        ResourceResolver resolver = null;
        try {
            resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
            String path = RONUtils.safeGetRequestParam("itemPath", request);
            String quantity = RONUtils.safeGetRequestParam("quantity", request);

            Resource product = resolver.getResource(path);
            if(product != null){
                RONProduct item = product.adaptTo(RONProduct.class);
                if(item != null){
                    int newQuantity = Integer.parseInt(quantity);
                    JsonObject returnObject = new JsonObject();
                    boolean canOrderNewQuantity = newQuantity <= Integer.parseInt(item.getQuantityAvailable());
                    returnObject.addProperty("isAllowed", canOrderNewQuantity);
                    if(canOrderNewQuantity){
                        returnObject.addProperty("quantity", newQuantity);
                        returnObject.addProperty("priceTotal", newQuantity * Double.parseDouble(item.getPrice()));
                    }else{
                        returnObject.addProperty("quantity", item.getQuantityAvailable());
                    }
                    response.setCharacterEncoding("UTF-8");
                    response.setContentType("application/json");
                    response.getWriter().write(returnObject.toString());
                    //new line item price (qty * price)
                    //flag for if we had to limit the order to a partial quantity
                }
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
