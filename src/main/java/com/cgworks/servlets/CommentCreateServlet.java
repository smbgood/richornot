package com.cgworks.servlets;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SlingServlet(paths="/bin/comment/create", methods="POST")
public class CommentCreateServlet extends SlingAllMethodsServlet {

    @Reference
    private
    ResourceResolverFactory rrf;

    @Override
    public void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response){
        String resourcePath = request.getRequestParameter("path").getString();
        String message = request.getRequestParameter("comment").getString();
        RequestParameter userPathParam = request.getRequestParameter("userPath");
        RequestParameter authorNameParam = request.getRequestParameter("authorName");
        String userPath = "";
        String authorName ="";
        if(userPathParam != null)
            userPath = userPathParam.getString();
        if(authorNameParam != null){
            authorName = authorNameParam.getString();
        }
        if (StringUtils.isNotBlank(message)) {
            Map<String, Object> props = new HashMap<String, Object>();
            props.put("jcr:primaryType", "nt:unstructured");
            props.put("sling:resourceType", "comment-data");
            props.put("message", message);
            if(StringUtils.isNotBlank(authorName))
                props.put("author", authorName);
            if(StringUtils.isNotBlank(userPath))
                props.put("userPath", userPath);
            Resource created = null;
            ResourceResolver resolver = null;
            try {
                resolver = rrf.getAdministrativeResourceResolver(null);
            } catch (LoginException e) {
                e.printStackTrace();
            }
            try {
                if(resolver == null){
                    try {
                        response.sendError(420);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return;
                }
                Map<String, Object> childProps = new HashMap<String, Object>();
                childProps.put("jcr:primaryType", "nt:unstructured");
                childProps.put("sling:resourceType", "data-parent");
                Resource dataChild = ResourceUtil.getOrCreateResource(resolver, resourcePath + "/data", childProps, null, true );
                created = resolver.create(dataChild, java.util.UUID.randomUUID() + "", props);
                try {
                    resolver.commit();
                } catch (PersistenceException e) {
                    e.printStackTrace();
                }
            } catch (PersistenceException e) {
                e.printStackTrace();
            }
            if (created != null) {
                try {
                    response.sendRedirect(resourcePath + ".html");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    response.sendError(420);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(resolver.isLive()){
                resolver.close();
            }
        }
    }
}
