package com.cgworks.components;

import com.cgworks.services.ImageSizingService;
import com.cgworks.slingmodels.RONRendition;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;

public class Image implements Use {

    private RONRendition rendition;

    public void init(Bindings bindings) {
        SlingScriptHelper helper = (SlingScriptHelper) bindings.get("sling");

        String width = (String) bindings.get("width");

        String name = (String) bindings.get("name");

        if(helper != null && StringUtils.isNotBlank(width) && StringUtils.isNotBlank(name)){
            ImageSizingService service = helper.getService(ImageSizingService.class);
            rendition = service.selectRenditionForNameAndWidth(name, Integer.parseInt(width));
        }
    }

    public RONRendition getRendition() {
        return rendition;
    }
}
