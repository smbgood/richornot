package com.cgworks.components;

import com.cgworks.data.RONCart;
import com.cgworks.data.RONItem;
import com.cgworks.services.UserCartService;
import com.cgworks.slingmodels.RONPage;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;
import javax.servlet.http.HttpSession;

public class CartWidget implements Use {

    private RONCart cart;

    private int cartSize;

    private String orderPageLink;

    public void init(Bindings bindings) {
        SlingScriptHelper sling = (SlingScriptHelper) bindings.get("sling");
        if(sling != null){
            UserCartService cartService = sling.getService(UserCartService.class);
            if(cartService != null){
                final SlingHttpServletRequest request = (SlingHttpServletRequest) bindings.get("request");
                String userToken = RONUtils.deObfuscateToken(RONUtils.safeGetCookie(Constants.USER_COOKIE_TOKEN_STRING, request));
                if(StringUtils.isNotBlank(userToken)){
                    cart = cartService.getUserCart(userToken);
                }else{
                    //check for preexisting guest token, otherwise
                    String guestToken = RONUtils.safeGetAttribute("guestToken", request);
                    RONCart guestCart = null;
                    if(StringUtils.isNotBlank(guestToken)){
                        guestCart = cartService.getUserCart(guestToken);
                    }else{
                        guestToken = RandomStringUtils.randomAlphanumeric(16);
                    }
                    if(guestCart != null){
                        cart = guestCart;
                    }else{
                        RONCart cart = new RONCart(guestToken, true);
                        cartService.updateCart(guestToken, cart);
                        HttpSession s = request.getSession();
                        if(s != null){
                            s.setAttribute("guestToken", guestToken);
                        }
                        this.cart = cart;
                    }
                    //make a new one and add to service and add cookie to request
                }
                final int[] cartSize = {0};
                if(cart != null){
                    cart.getItems().keySet().forEach(one -> cartSize[0] += cart.getItems().get(one).getQuantity());
                }
                this.cartSize = cartSize[0];
                RONPage checkoutPage = RONUtils.findPageByResourceType("components/order", request);
                if(checkoutPage != null){
                    this.orderPageLink = checkoutPage.getResourcePath() + ".html";
                }
            }
        }
    }

    public RONCart getCart() {
        return cart;
    }

    public int getCartSize() {
        return cartSize;
    }

    public String getOrderPageLink() {
        return orderPageLink;
    }
}
