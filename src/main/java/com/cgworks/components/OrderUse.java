package com.cgworks.components;

import com.cgworks.data.RONCart;
import com.cgworks.data.RONItem;
import com.cgworks.services.UserCartService;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import com.google.gson.Gson;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;
import java.util.ArrayList;
import java.util.Collection;


public class OrderUse implements Use {

    protected RONCart userCart;

    protected ArrayList<RONItem> items;

    protected RONCart guestCart;

    public RONCart getUserCart() {
        return userCart;
    }

    public ArrayList<RONItem> getItems() {
        return items;
    }

    public RONCart getGuestCart() {
        return guestCart;
    }

    @Override
    public void init(Bindings bindings) {
        SlingScriptHelper helper = (SlingScriptHelper) bindings.get("sling");
        if(helper != null){
            UserCartService cartService = helper.getService(UserCartService.class);
            if(cartService != null){
                final SlingHttpServletRequest request = (SlingHttpServletRequest) bindings.get("request");
                if(request != null){
                    String userToken = RONUtils.deObfuscateToken(RONUtils.safeGetCookie(Constants.USER_COOKIE_TOKEN_STRING, request));
                    if(StringUtils.isNotBlank(userToken)){
                        userCart = cartService.getUserCart(userToken);
                        items = getItemsFromCart(userCart);
                    }else{
                        userCart = null;
                        guestCart = cartService.getUserCart(RONUtils.safeGetAttribute(Constants.GUEST_COOKIE_TOKEN_STRING, request));
                        items = getItemsFromCart(guestCart);
                    }

                }
            }
        }
    }

    private ArrayList<RONItem> getItemsFromCart(RONCart userCart) {
        final ArrayList<RONItem> ronItems = new ArrayList<RONItem>();
        if(userCart != null && userCart.getItems() != null) {
            final Collection<RONItem> values = userCart.getItems().values();
            if (CollectionUtils.isNotEmpty(values))
                ronItems.addAll(values);
        }
        return ronItems;
    }

    public String getJson(){
        if(userCart != null)
            return new Gson().toJson(userCart, RONCart.class);
        else
            return new Gson().toJson(guestCart, RONCart.class);
    }
}
