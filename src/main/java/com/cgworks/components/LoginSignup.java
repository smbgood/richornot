package com.cgworks.components;

import com.cgworks.data.RONAccount;
import com.cgworks.services.RONAccountManager;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;

public class LoginSignup implements Use {

    private boolean loggedIn;

    private RONAccount user;

    public boolean isLoggedIn(){
        return loggedIn;
    }

    public void init(Bindings bindings) {
        user = RONUtils.getUserAccountForUse(bindings);
        if(user != null && user.getUserName() != null){
            loggedIn = true;
        }
    }

    public RONAccount getUser() {
        return user;
    }
}
