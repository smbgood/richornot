package com.cgworks.components.page;

import com.cgworks.data.RONAccount;
import com.cgworks.services.RONAccountManager;
import com.cgworks.slingmodels.RONUser;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;
import java.util.ArrayList;

public class AdminPage implements Use {

    private boolean canAccess;

    private ArrayList<RONAccount> userAccounts;

    public void init(Bindings bindings) {
        String userToken = RONUtils.deObfuscateToken(RONUtils.safeGetCookie(Constants.USER_COOKIE_TOKEN_STRING, (SlingHttpServletRequest) bindings.get("request")));
        if(StringUtils.isNotBlank(userToken)){
            RONAccountManager accountManager = ((SlingScriptHelper) bindings.get("sling")).getService(RONAccountManager.class);
            if(accountManager != null) {
                RONAccount user = accountManager.getAccountFromToken(userToken);
                if (user != null && user.isAdmin()) {
                    canAccess = true;
                    userAccounts = (ArrayList<RONAccount>) accountManager.getUserList();
                }
            }
        }
    }

    public boolean getCanAccess() {
        return canAccess;
    }

    public ArrayList<RONAccount> getUserAccounts() {
        return userAccounts;
    }
}
