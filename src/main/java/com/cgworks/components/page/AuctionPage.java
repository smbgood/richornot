package com.cgworks.components.page;

import com.cgworks.services.AuctionItemService;
import com.cgworks.slingmodels.RONAuctionItem;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;
import java.util.ArrayList;

public class AuctionPage implements Use {

    public ArrayList<RONAuctionItem> getAuctionItems() {
        return auctionItems;
    }

    private ArrayList<RONAuctionItem> auctionItems;

    @Override
    public void init(Bindings bindings) {
        SlingScriptHelper sling = (SlingScriptHelper) bindings.get("sling");
        if(sling != null) {
            AuctionItemService auctionService = sling.getService(AuctionItemService.class);
            if (auctionService != null) {
                auctionItems = (ArrayList<RONAuctionItem>) auctionService.getItemsForAuction();
            }
        }
    }


}
