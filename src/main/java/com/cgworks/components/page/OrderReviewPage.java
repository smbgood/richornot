package com.cgworks.components.page;

import com.cgworks.data.RONCart;
import com.cgworks.services.UserCartService;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;

public class OrderReviewPage implements Use {

    private RONCart userCart;
    private RONCart guestCart;

    private boolean canSubmit = false;

    @Override
    public void init(Bindings bindings) {
        SlingScriptHelper helper = (SlingScriptHelper) bindings.get("sling");
        if(helper != null){
            UserCartService cartService = helper.getService(UserCartService.class);
            if(cartService != null){
                final SlingHttpServletRequest request = (SlingHttpServletRequest) bindings.get("request");
                if(request != null){
                    String userToken = RONUtils.deObfuscateToken(RONUtils.safeGetCookie(Constants.USER_COOKIE_TOKEN_STRING, request));
                    if(StringUtils.isNotBlank(userToken)){
                        userCart = cartService.getUserCart(userToken);
                        if(userCart != null)
                            canSubmit = CollectionUtils.isNotEmpty(userCart.getOrderableItems()) || CollectionUtils.isNotEmpty(userCart.getPartialItems());
                    }else{
                        userCart = null;
                        guestCart = cartService.getUserCart(RONUtils.safeGetAttribute(Constants.GUEST_COOKIE_TOKEN_STRING, request));
                        if(guestCart != null)
                            canSubmit = CollectionUtils.isNotEmpty(guestCart.getOrderableItems()) || CollectionUtils.isNotEmpty(guestCart.getPartialItems());
                    }

                }
            }
        }
    }

    public String getJson(){
        if(userCart != null)
            return new Gson().toJson(userCart, RONCart.class);
        else
            return new Gson().toJson(guestCart, RONCart.class);
    }

    public RONCart getUserCart() {
        return userCart;
    }

    public RONCart getGuestCart() {
        return guestCart;
    }

    public boolean isCanSubmit() {
        return canSubmit;
    }
}
