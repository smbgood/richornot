package com.cgworks.components.page;

import com.cgworks.data.RONAccount;
import com.cgworks.services.OrderManager;
import com.cgworks.slingmodels.RONOrder;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;
import java.util.ArrayList;

public class AccountPage implements Use{

    public ArrayList<RONOrder> getOrderList() {
        return orderList;
    }

    private ArrayList<RONOrder> orderList;

    //todo redirect if user not logged in

    public void init(Bindings bindings) {
        orderList = new ArrayList<RONOrder>();
        RONAccount user = RONUtils.getUserAccountForUse(bindings);
        if(user != null){
            String userPath = user.getUserPath();
            if(StringUtils.isNotBlank(userPath)){
                SlingScriptHelper helper = (SlingScriptHelper) bindings.get("sling");
                OrderManager orderManager = helper.getService(OrderManager.class);
                if(orderManager != null){
                    orderList = (ArrayList<RONOrder>) orderManager.getOrdersForUserPath(userPath);
                }
            }
        }
    }
}
