package com.cgworks.components.page;

import com.cgworks.data.RONAccount;
import com.cgworks.slingmodels.RONOrder;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;

public class OrderDetailPage implements Use {

    public RONOrder getCurrentOrder() {
        return currentOrder;
    }

    private RONOrder currentOrder;

    public void init(Bindings bindings) {
        Resource currentResource = (Resource) bindings.get("resource");

        RONAccount currentUser = RONUtils.getUserAccountForUse(bindings);

        if(currentResource != null && currentUser != null){
            RONOrder order = currentResource.adaptTo(RONOrder.class);
            if(order != null){
                if(!order.isGuestOrder()){
                    String userOrdered = order.getUserOrdered();
                    if(StringUtils.isNotBlank(userOrdered) && userOrdered.equals(currentUser.getUserPath())){
                        currentOrder = order;
                    }
                }
            }
        }


    }
}
