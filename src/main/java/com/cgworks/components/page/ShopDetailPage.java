package com.cgworks.components.page;

import com.cgworks.slingmodels.RONProduct;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;

public class ShopDetailPage implements Use {

    private RONProduct currentProduct;

    public void init(Bindings bindings) {
        Resource r = (Resource) bindings.get("resource");
        if(r != null){
            currentProduct = r.adaptTo(RONProduct.class);
        }
    }

    public RONProduct getCurrentProduct() {
        return currentProduct;
    }
}
