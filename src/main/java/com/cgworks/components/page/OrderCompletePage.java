package com.cgworks.components.page;

import com.cgworks.services.OrderManager;
import com.cgworks.slingmodels.RONOrder;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;

public class OrderCompletePage implements Use{

    private String orderStatus;

    @Override
    public void init(Bindings bindings) {
        //this will be post-checkout, to confirm to the user their order went through
        SlingHttpServletRequest request = (SlingHttpServletRequest) bindings.get("request");
        if(request != null){
            String orderId = request.getQueryString() != null ? request.getQueryString().replace("order=", "") : "";
            if(StringUtils.isNotBlank(orderId)) {
                SlingScriptHelper slingScriptHelper = (SlingScriptHelper) bindings.get("sling");
                if (slingScriptHelper != null) {
                    OrderManager om = slingScriptHelper.getService(OrderManager.class);
                    if (om != null) {
                        RONOrder order = om.getOrderForId(orderId);
                        if (order != null)
                            orderStatus = order.getOrderStatus();
                    }
                }
            }
        }
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
