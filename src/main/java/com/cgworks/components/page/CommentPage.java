package com.cgworks.components.page;

import com.cgworks.data.RONAccount;
import com.cgworks.services.CommentService;
import com.cgworks.slingmodels.RONComment;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;
import java.util.ArrayList;
import java.util.HashMap;

public class CommentPage implements Use {

    private HashMap<String, HashMap<String, RONComment>> currentPathMappings;

    private String currentAuthor;

    private String resourcePath;

    private String userCommentingPath;

    public void init(Bindings bindings) {

        SlingScriptHelper helper = (SlingScriptHelper) bindings.get("sling");
        if(helper != null){
            CommentService commentService = helper.getService(CommentService.class);
            if(commentService != null) {
                currentPathMappings = commentService.getPathMappings();
            }
        }

        Resource r = (Resource) bindings.get("resource");
        setResourcePath(r.getPath());

        RONAccount account = (RONAccount) bindings.get("account");
        if(account != null){
            userCommentingPath = account.getUserPath();
        }

        String author = (String) bindings.get("author");
        if(author != null){
            currentAuthor = author;
        }
    }

    public ArrayList<RONComment> getOutput() {
        if(currentPathMappings != null){
            if(currentPathMappings.containsKey(resourcePath)) {
                HashMap<String, RONComment> comments = currentPathMappings.get(resourcePath);
                ArrayList<RONComment> outList = new ArrayList<RONComment>();
                for(String s : comments.keySet()){
                    outList.add(comments.get(s));
                }
                return outList;
            }
        }

        return null;
    }

    public String getCurrentAuthor() {
        return currentAuthor;
    }

    public String getUserCommentingPath() {
        return userCommentingPath;
    }

    public String getResourcePath(){
        return resourcePath;
    }

    public void setResourcePath(String path){
        resourcePath = path;
    }
}
