package com.cgworks.components.page;

import com.cgworks.services.ShopItemService;
import com.cgworks.slingmodels.RONProduct;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;
import java.util.ArrayList;
import java.util.HashSet;


public class ShopPage implements Use {

    private ArrayList<RONProduct> forSaleItems;

    private HashSet<String> filtersAvailable;

    public void init(Bindings bindings) {
        filtersAvailable = new HashSet<String>();
        SlingScriptHelper sling = (SlingScriptHelper) bindings.get("sling");
        if(sling != null){
            ShopItemService shopService = sling.getService(ShopItemService.class);
            if(shopService != null){
                forSaleItems = (ArrayList<RONProduct>) shopService.getProductsForSale();
                for(RONProduct p : forSaleItems){
                    if(p != null) {
                        String category = p.getCategory();
                        if(StringUtils.isNotBlank(category))
                            filtersAvailable.add(category);
                    }
                }
            }
        }

        SlingHttpServletRequest request = (SlingHttpServletRequest) bindings.get("request");
        String filterParam = (String) request.getQueryString();
        if(StringUtils.isNotBlank(filterParam)){
            if(filterParam.startsWith("searchInput")) {
                filterParam = filterParam.replace("+", " ");
                filterParam = filterParam.substring(12);
            }
            ArrayList<RONProduct> outProducts = new ArrayList<RONProduct>();
            for(RONProduct product : forSaleItems){
                if(product == null)
                    continue;
                String title = product.getTitle();
                String category = product.getCategory();
                if(filterParam.contains(category) || title.contains(filterParam) ){
                    outProducts.add(product);
                }
            }
            forSaleItems = outProducts;
        }
    }

    public ArrayList<RONProduct> getForSaleItems() {
        return forSaleItems;
    }

    public ArrayList<String> getFiltersAvailable() {
        ArrayList<String> ronProducts = new ArrayList<String>();
        ronProducts.addAll(filtersAvailable);
        return ronProducts;
    }
}
