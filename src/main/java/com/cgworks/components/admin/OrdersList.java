package com.cgworks.components.admin;

import com.cgworks.services.OrderManager;
import com.cgworks.slingmodels.RONOrder;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;
import java.util.ArrayList;

public class OrdersList implements Use {

    public ArrayList<RONOrder> getOrderList() {
        return orderList;
    }

    private ArrayList<RONOrder> orderList;

    public void init(Bindings bindings) {
        orderList = new ArrayList<RONOrder>();
        SlingScriptHelper helper = (SlingScriptHelper) bindings.get("sling");
        if(helper != null){
            OrderManager orderManager = helper.getService(OrderManager.class);
            if(orderManager != null){
                orderList = (ArrayList<RONOrder>) orderManager.getOrders();
            }
        }
    }
}
