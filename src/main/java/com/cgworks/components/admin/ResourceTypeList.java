package com.cgworks.components.admin;

import com.cgworks.data.admin.RONResourceType;
import com.cgworks.services.admin.ResourceTypeService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;
import java.util.ArrayList;

public class ResourceTypeList implements Use{

    public ArrayList<RONResourceType> getAvailableRTypes() {
        return availableRTypes;
    }

    private ArrayList<RONResourceType> availableRTypes;

    public void init(Bindings bindings) {
        final SlingScriptHelper sling = (SlingScriptHelper) bindings.get("sling");
        ResourceTypeService typeService = (ResourceTypeService) sling.getService(ResourceTypeService.class);
        this.availableRTypes = typeService.getAvailablePageResourceTypes();
    }
}
