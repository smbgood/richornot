package com.cgworks.components;

import com.cgworks.data.RONNavigation;
import com.cgworks.services.SiteNavService;
import com.cgworks.utils.Constants;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;
import java.net.MalformedURLException;
import java.net.URL;


public class SiteNav implements Use {


    public RONNavigation getNavigation() {
        return navigation;
    }

    private RONNavigation navigation;

    public void init(Bindings bindings) {
        SlingScriptHelper sling = (SlingScriptHelper) bindings.get("sling");
        if(sling != null){
            SiteNavService siteNav = sling.getService(SiteNavService.class);
            if(siteNav != null){
                SlingHttpServletRequest request = (SlingHttpServletRequest) bindings.get("request");
                if(request != null){
                    String path = request.getResource().getPath();
                    navigation = siteNav.getSiteNavigationInfoForPath(path);
                    String backPath = request.getHeader("Referer");
                    //TODO check if we are within RON domain or not
                    String resourcePath = "";
                    if(StringUtils.isNotBlank(backPath)) {
                        try {
                            URL url = new URL(backPath);
                            if (url != null) {
                                resourcePath = url.getPath().replace(Constants.HTML_SUFFIX, "");
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                    if(StringUtils.isNotBlank(resourcePath))
                        navigation.setBackPage(resourcePath, request);
                }
            }
        }

    }
}
