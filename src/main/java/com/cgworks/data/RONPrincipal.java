package com.cgworks.data;

import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import javax.security.auth.Subject;
import java.security.Principal;

public class RONPrincipal implements Principal {

    private String userToken = "";

    private String seed;

    private String userEmail;

    private String seedHex;

    public RONPrincipal(String email, String seed) {
        this.userEmail = email;
        this.seed = seed;
    }

    public String getName() {
        if(StringUtils.isBlank(userToken)){
            initCryptoDetails();
        }
        return userToken;
    }

    public String getSeedHex() {
        if(StringUtils.isBlank(seedHex)){
            initCryptoDetails();
        }
        return seedHex;
    }

    public String getSeed() {
        if(StringUtils.isBlank(seed)){
            initCryptoDetails();
        }
        return seed;
    }

    private void initCryptoDetails() {
        if(StringUtils.isBlank(seed))
            seed = RandomStringUtils.randomAlphanumeric(32);
        seedHex = RONUtils.toHex(seed);
        userToken = RONUtils.getObfuscatedTokenStringForAlphaString(seedHex);
    }

    public String getUserEmail() {
        return userEmail;
    }

    public boolean implies(Subject subject) {
        return false;
    }
}
