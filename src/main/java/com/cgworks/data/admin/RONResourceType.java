package com.cgworks.data.admin;

import com.cgworks.data.jaxb.Template;

public class RONResourceType {

    public String getResourceType() {
        return resourceType;
    }

    public Template getTemplate() {
        return template;
    }

    private String resourceType;

    private Template template;

    public RONResourceType(String resourceType, Template t) {
        this.template = t;
        this.resourceType = resourceType;
    }
}
