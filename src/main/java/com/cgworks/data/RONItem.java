package com.cgworks.data;

import com.cgworks.slingmodels.RONProduct;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.sling.api.resource.Resource;

import java.io.Serializable;

public class RONItem implements Serializable {

    private String productPath;

    public RONProduct getProduct() {
        return product;
    }

    private RONProduct product;

    public RONItem(String productPath, int quantity, Resource resource) {
        this.productPath = productPath;
        this.quantity = quantity;
        if(resource != null)
            this.product = resource.adaptTo(RONProduct.class);

    }

    public String getProductPath() {
        return productPath;
    }

    public void setProductPath(String productPath) {
        this.productPath = productPath;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    private int quantity;

    public JsonObject getJson(){
        JsonObject j = new JsonObject();
        j.add("productPath", new JsonPrimitive(this.productPath));
        j.add("quantity", new JsonPrimitive(this.quantity));
        return j;
    }
}
