package com.cgworks.data;


public class OrderStatus {
    public static final String CREATED = "created";
    public static final String NOTIFIED = "notified";
    public static final String PAID = "paid";
    public static final String SHIPPED = "shipped";
    public static final String COMPLETE = "complete";
}
