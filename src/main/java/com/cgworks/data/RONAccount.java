package com.cgworks.data;

import com.cgworks.slingmodels.RONUser;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.Impersonation;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import javax.jcr.Credentials;
import javax.jcr.RepositoryException;
import javax.jcr.UnsupportedRepositoryOperationException;
import javax.jcr.Value;
import java.security.Principal;
import java.util.Iterator;

public class RONAccount implements User {

    private String userToken;

    public String getUserName() {
        return userName;
    }

    private String userName;

    public String getUserPath() {
        return userPath;
    }

    private String userPath;

    public String getUserEmail() { return userEmail; }

    private String userEmail;

    private boolean isAdmin;

    public RONPrincipal getUserPrincipal() {
        return userPrincipal;
    }

    private RONPrincipal userPrincipal;

    public RONAccount(Authorizable authorizable, String emailAddress) {
        try {
            userName = authorizable.getID();
            userPath = authorizable.getPath();
            userEmail = emailAddress;
            userPrincipal = new RONPrincipal(emailAddress, "");
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    public RONAccount(Authorizable authorizable, ResourceResolver resolver){
        try {
            userName = authorizable.getID();
            userPath = authorizable.getPath();
            String seed = "";
            if(StringUtils.isNotBlank(userPath)){
                Resource userResource = resolver.getResource(userPath);
                if(userResource != null){
                    RONUser userModel = userResource.adaptTo(RONUser.class);
                    if(userModel != null){
                        userEmail = userModel.getEmail();
                        seed = userModel.getSeed();
                    }
                }
            }
            userPrincipal = new RONPrincipal(userEmail, seed);
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public boolean isSystemUser() {
        return false;
    }

    public Credentials getCredentials() throws RepositoryException {
        return null;
    }

    public Impersonation getImpersonation() throws RepositoryException {
        return null;
    }

    public void changePassword(String s) throws RepositoryException {

    }

    public void changePassword(String s, String s1) throws RepositoryException {

    }

    public void disable(String s) throws RepositoryException {

    }

    public boolean isDisabled() throws RepositoryException {
        return false;
    }

    public String getDisabledReason() throws RepositoryException {
        return null;
    }

    public String getID() throws RepositoryException {
        return null;
    }

    public boolean isGroup() {
        return false;
    }

    public Principal getPrincipal() throws RepositoryException {
        return userPrincipal;
    }

    public Iterator<Group> declaredMemberOf() throws RepositoryException {
        return null;
    }

    public Iterator<Group> memberOf() throws RepositoryException {
        return null;
    }

    public void remove() throws RepositoryException {

    }

    public Iterator<String> getPropertyNames() throws RepositoryException {
        return null;
    }

    public Iterator<String> getPropertyNames(String s) throws RepositoryException {
        return null;
    }

    public boolean hasProperty(String s) throws RepositoryException {
        return false;
    }

    public void setProperty(String s, Value value) throws RepositoryException {

    }

    public void setProperty(String s, Value[] values) throws RepositoryException {

    }

    public Value[] getProperty(String s) throws RepositoryException {
        return new Value[0];
    }

    public boolean removeProperty(String s) throws RepositoryException {
        return false;
    }

    public String getPath() throws UnsupportedRepositoryOperationException, RepositoryException {
        return null;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
}
