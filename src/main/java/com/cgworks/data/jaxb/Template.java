package com.cgworks.data.jaxb;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Template {

    public boolean isPageType() {
        return isPageType;
    }

    public boolean isRequiresAdmin() {
        return requiresAdmin;
    }

    public boolean isRequiresUAC() {
        return requiresUAC;
    }

    @XmlElement
    private boolean isPageType;

    @XmlElement
    private boolean requiresAdmin;

    @XmlElement
    private boolean requiresUAC;

    public boolean isHideFromNav() {
        return hideFromNav;
    }

    @XmlElement
    private boolean hideFromNav;
}
