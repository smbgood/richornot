package com.cgworks.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.sling.api.resource.Resource;

import java.util.ArrayList;
import java.util.HashMap;

public class RONCart {

    public boolean isGuest() {
        return isGuest;
    }

    private final boolean isGuest;

    public HashMap<String, RONItem> getItems() {
        return items;
    }

    private HashMap<String, RONItem> items;

    public ArrayList<RONItem> getOrderableItems() {
        return orderableItems;
    }

    public void setOrderableItems(ArrayList<RONItem> orderableItems) {
        this.orderableItems = orderableItems;
    }

    public ArrayList<RONItem> getPartialItems() {
        return partialItems;
    }

    public void setPartialItems(ArrayList<RONItem> partialItems) {
        this.partialItems = partialItems;
    }

    public ArrayList<RONItem> getNullItems() {
        return nullItems;
    }

    public void setNullItems(ArrayList<RONItem> nullItems) {
        this.nullItems = nullItems;
    }

    private ArrayList<RONItem> orderableItems;
    private ArrayList<RONItem> partialItems;
    private ArrayList<RONItem> nullItems;

    private long timestamp;

    private String tokenId;

    public RONCart(String tokenId, boolean isGuest){
        items = new HashMap<String, RONItem>();
        timestamp = System.currentTimeMillis();
        this.isGuest = isGuest;
        this.tokenId = tokenId;
    }

    public void addItem(Resource resource, int quantity) {
        if(resource == null)
            return;
        String productPath = resource.getPath();
        if(items.containsKey(productPath)){
            RONItem item = items.get(productPath);
            item.setQuantity(item.getQuantity() + quantity);
            items.put(productPath, item);
        }else{
            items.put(productPath, new RONItem(productPath, quantity, resource));
        }
    }

    public String getGrandTotal(){
        double total = 0.0d;
        if(CollectionUtils.isEmpty(getOrderableItems()) && CollectionUtils.isEmpty(getPartialItems()) && CollectionUtils.isEmpty(getNullItems())){
            for(RONItem item : items.values()){
                if(item.getProduct() != null)
                    total += (item.getQuantity() * Double.parseDouble(item.getProduct().getPrice()));
            }
        }else {
            for (RONItem item : getOrderableItems()) {
                if (item.getProduct() != null)
                    total += (item.getQuantity() * Double.parseDouble(item.getProduct().getPrice()));
            }
            for (RONItem item : getPartialItems()) {
                if (item.getProduct() != null)
                    total += (item.getQuantity() * Double.parseDouble(item.getProduct().getPrice()));
            }
        }
        return String.valueOf(total);
    }

    public String getJson(){
        JsonObject root = new JsonObject();
        if(MapUtils.isNotEmpty(items)) {
            ArrayList<RONItem> orderItems = new ArrayList<RONItem>();
            orderItems.addAll(items.values());
            JsonArray array = new JsonArray();
            for(RONItem item : orderItems){
                array.add(item.getJson());
            }
            root.add("items", array);
        }

        JsonElement je = new JsonPrimitive(isGuest);
        root.add("isGuest", je);

        return root.toString();
    }

    public String getOrderJson(){
        JsonObject root = new JsonObject();
        if(CollectionUtils.isNotEmpty(orderableItems)) {
            JsonArray array = new JsonArray();
            for(RONItem item : orderableItems){
                array.add(item.getJson());
            }
            root.add("orderableItems", array);
        }
        if(CollectionUtils.isNotEmpty(partialItems)) {
            JsonArray array = new JsonArray();
            for(RONItem item : partialItems){
                array.add(item.getJson());
            }
            root.add("partialItems", array);
        }
        if(CollectionUtils.isNotEmpty(nullItems)) {
            JsonArray array = new JsonArray();
            for(RONItem item : nullItems){
                array.add(item.getJson());
            }
            root.add("nullItems", array);
        }

        JsonElement je = new JsonPrimitive(isGuest);
        root.add("isGuest", je);

        return root.toString();
    }

    public String getTokenId() {
        return tokenId;
    }


}
