package com.cgworks.data;


import com.cgworks.slingmodels.RONPage;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

import java.util.ArrayList;
import java.util.Iterator;

public class RONNavigation {

    private RONPage currentPage;

    private ArrayList<RONNavigation> childPages;

    private ArrayList<RONNavigation> authRequiredPages;

    private RONPage backPage;

    private boolean isActiveNavItem;

    public RONNavigation(Resource root){
        //if building /content, skip, need to go up a level

        //if building /content/home childPages will hold home,shop,cart

        //if building /content/home/promo
        // and also have /content/home/promo2, childPage will hold promo, promo2

        //get child items, attempt to adapt to RONPage,
        if(root != null){
            //if adaptable to ronpage, on a child, otherwise on root
            RONPage thisPage = root.adaptTo(RONPage.class);
            if(thisPage != null){
                currentPage = thisPage;
            }
            final Iterable<Resource> children = root.getChildren();
            ArrayList<RONNavigation> navigations = new ArrayList<RONNavigation>();
            ArrayList<RONNavigation> authorizedNavigations = new ArrayList<RONNavigation>();
            for (Resource next : children) {
                if(next == null)
                    continue;
                RONPage p = next.adaptTo(RONPage.class);
                if (p != null && StringUtils.isNotBlank(p.getResourceType()) && p.getResourceType().contains("components/")) {
                    if (p.getHideFromNav())
                        continue;
                    if (Boolean.parseBoolean(p.getRequiresAuth())) {
                        authorizedNavigations.add(new RONNavigation(next));
                    } else {
                        navigations.add(new RONNavigation(next));
                    }
                }
            }
            childPages = navigations;
            authRequiredPages = authorizedNavigations;
        }
    }

    public RONPage getCurrentPage() {
        return currentPage;
    }

    public ArrayList<RONNavigation> getChildPages() {
        return childPages;
    }

    public ArrayList<RONNavigation> getAuthRequiredPages() {
        return authRequiredPages;
    }

    public RONPage getBackPage() {
        return backPage;
    }

    public boolean isActiveNavItem() {
        return isActiveNavItem;
    }

    public void setActiveNavItem(boolean activeNavItem) {
        isActiveNavItem = activeNavItem;
    }

    public void setActiveItem(String path) {
        // OLD path like /content/home
        // new path like /content/en/home

        //assuming we are root level here
        int count = StringUtils.countMatches(path, "/");
        int i = 1;
        int lastSlashIndex = path.indexOf("/");
        while(i < count){
            i++;

            final int endIndex = StringUtils.ordinalIndexOf(path, "/", i);
            String namePart = path.substring(lastSlashIndex, endIndex);
            lastSlashIndex = endIndex;

            ArrayList<RONNavigation> childs = this.getChildPages();
            for(RONNavigation child : childs){
                final String resourcePath = child.getCurrentPage().getResourcePath();
                if(resourcePath.equals(path) || resourcePath.endsWith(namePart)){
                    child.setActiveNavItem(true);
                }
                child.setActiveItem(path);
            }
        }
    }

    public void setBackPage(String backPage, SlingHttpServletRequest request) {
        Resource page = request.getResourceResolver().getResource(backPage);
        if(page != null){
            RONPage ronPage = page.adaptTo(RONPage.class);
            if(ronPage != null){
                this.backPage = ronPage;
            }
        }
    }
}
