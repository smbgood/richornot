package com.cgworks.services;

import com.cgworks.slingmodels.RONImage;
import com.cgworks.slingmodels.RONRendition;

import javax.jcr.observation.EventListener;

public interface ImageSizingService {
    //when image uploaded, create some rendition data below for our necessary sizes

    //component use obj is passed normal image path, and a display width possibly, then selects a rendition

    RONRendition selectRenditionForNameAndWidth(String path, int width);


}
