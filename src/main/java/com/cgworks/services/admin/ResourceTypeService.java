package com.cgworks.services.admin;

import com.cgworks.data.admin.RONResourceType;

import java.util.ArrayList;
import java.util.HashMap;

public interface ResourceTypeService {

    ArrayList<RONResourceType> getAvailablePageResourceTypes();

    HashMap<String, Object> getPagePropsForRType(String resourceType);

}
