package com.cgworks.services.admin.impl;

import com.cgworks.data.admin.RONResourceType;
import com.cgworks.data.jaxb.Template;
import com.cgworks.services.admin.ResourceTypeService;
import com.cgworks.utils.RONUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.Binary;
import javax.jcr.RepositoryException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

@Component(immediate = true)
@Service
public class ResourceTypeServiceImpl implements ResourceTypeService {

    @Reference
    private ResourceResolverFactory rrf;

    @Reference
    private SlingRepository repo;

    private ArrayList<RONResourceType> pagesAvailable;

    private HashMap<String, RONResourceType> typeHashMap;

    @Activate
    public void activate(){
        pagesAvailable = new ArrayList<RONResourceType>();
        typeHashMap = new HashMap<String, RONResourceType>();
        //iterate through /apps/components
        //if we have an info.xml, parse it and add to types
        try {
            ResourceResolver resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
            Resource rootComponents = resolver.getResource("/apps/components");
            Iterable<Resource> resources = rootComponents.getChildren();
            for(Resource r : resources){
                String resourceType = r.getPath();
                resourceType = resourceType.replace("/apps/", "");
                final Resource infoFile = r.getChild("info.xml");
                if(infoFile != null){
                    try {
                        JAXBContext context = JAXBContext.newInstance(Template.class);
                        Unmarshaller unmarshaller = context.createUnmarshaller();
                        final InputStream is = (InputStream) infoFile.getChild("jcr:content").getValueMap().get("jcr:data");
                        Template t = (Template) unmarshaller.unmarshal(is);
                        if(t != null){
                            RONResourceType type = new RONResourceType(resourceType, t);
                            pagesAvailable.add(type);
                            typeHashMap.put(resourceType, type);
                        }
                        is.close();
                    } catch (JAXBException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<RONResourceType> getAvailablePageResourceTypes() {
        return pagesAvailable;
    }

    public HashMap<String, Object> getPagePropsForRType(String resourceType) {
        HashMap<String, Object> propsMap = new HashMap<String, Object>();
        if(typeHashMap.containsKey(resourceType)){
            RONResourceType type = typeHashMap.get(resourceType);
            if(type != null){
                Template t = type.getTemplate();
                if(t != null){
                    propsMap.put("hideFromNav", String.valueOf(t.isHideFromNav()));
                    propsMap.put("requiresAuth", String.valueOf(t.isRequiresUAC()));
                    propsMap.put("requiresAdmin", String.valueOf(t.isRequiresAdmin()));
                }
            }
        }
        return propsMap;
    }
}
