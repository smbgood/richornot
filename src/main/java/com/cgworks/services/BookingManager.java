package com.cgworks.services;

import com.cgworks.slingmodels.booking.RONMonth;

import java.util.ArrayList;

public interface BookingManager {

    ArrayList<RONMonth> getMonths();

}
