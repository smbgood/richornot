package com.cgworks.services;


import javax.jcr.RepositoryException;
import javax.mail.MessagingException;

public interface EmailMessagingService {

    void sendMessage(String type, String subject, String body, String recipient, String sender) throws MessagingException, RepositoryException;

}
