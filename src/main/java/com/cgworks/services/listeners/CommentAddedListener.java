package com.cgworks.services.listeners;

import com.cgworks.services.CommentService;
import com.cgworks.slingmodels.RONComment;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Service;

import org.apache.sling.api.resource.*;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import javax.jcr.*;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;
import org.apache.sling.jcr.api.SlingRepository;

import org.apache.felix.scr.annotations.Reference;

//Sling Imports


/**
 * Just a simple DS Component
 */
@Component(immediate=true)
@Service
public class CommentAddedListener implements EventListener {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private BundleContext bundleContext;

    @Reference
    private SlingRepository repository;

    //Inject a Sling ResourceResolverFactory
    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private CommentService commentService;

    private Session session;

    private ObservationManager observationManager;

    public void run() {
        log.info("Running...");
    }

    protected void activate(ComponentContext ctx) {
        this.bundleContext = ctx.getBundleContext();

        try
        {

            //Invoke the adaptTo method to create a Session
            // ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
            session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));

            // Setup the event handler to respond to a new claim under content/claim....
            observationManager = session.getWorkspace().getObservationManager();
            final String[] types = { "nt:unstructured"};
            final String path = Constants.CONTENT_ROOT; // define the path
            observationManager.addEventListener(this, Event.NODE_ADDED, path, true, null, null, false);
            //observationManager.addEventListener(this, Event.NODE_REMOVED, path, true, null, null, false);
            log.info("Observing node adds to {} nodes under {}", Arrays.asList(types), path);

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Deactivate
    protected void deactivate(ComponentContext componentContext) throws RepositoryException {

        if(observationManager != null) {
            observationManager.removeEventListener(this);
        }
        if (session != null) {
            session.logout();
            session = null;
        }
    }

    public void onEvent(EventIterator itr) {
        try {
            String path = itr.nextEvent().getPath();
            if(!path.contains("data/"))
                return;
            log.info("A new node was added to {}", path);
            ResourceResolver resolver = RONUtils.loginWithAdminCredentials(session, resolverFactory);
            if(resolver != null){
                Resource r = resolver.getResource(path);
                if(r != null && r.isResourceType("comment-data")){
                    while(!r.getParent().getParent().getResourceType().equals("components/comments")){
                        r = r.getParent();
                    }
                    //adapt parent comment resource and re-add to path mappings in comment service
                    //may be less performance-effective than traversing to the child
                    //but to do that escapes me for now
                    RONComment comment = r.adaptTo(RONComment.class);
                    if(comment != null) {
                        commentService.updateCommentsForPath(comment, resolver);
                    }
                }
            }
            if(resolver != null && resolver.isLive()){
                resolver.close();
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }

    }
}
