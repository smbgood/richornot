package com.cgworks.services;

import com.cgworks.slingmodels.RONAuctionItem;

import java.util.List;

public interface AuctionItemService {

    List<RONAuctionItem> getItemsForAuction();
}
