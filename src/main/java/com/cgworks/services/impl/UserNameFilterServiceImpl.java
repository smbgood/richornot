package com.cgworks.services.impl;

import com.cgworks.services.UserNameFilterService;
import com.cgworks.utils.RONUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.Binary;
import javax.jcr.RepositoryException;
import com.cgworks.utils.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

@Component(immediate = true)
@Service
public class UserNameFilterServiceImpl implements UserNameFilterService {


    private HashMap<String, String> swearsMap;

    @Reference
    private ResourceResolverFactory rrf;

    @Reference
    private SlingRepository repo;

    @Activate
    public void activate() throws RepositoryException {
        swearsMap = new HashMap<String, String>();
        ResourceResolver resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
        if(resolver != null){
            Resource swearsResource = resolver.getResource(Constants.USERNAME_FILTER_FILE_PATH);
            if(swearsResource != null){
                Resource jcrContent = swearsResource.getChild(JcrConstants.JCR_CONTENT);
                if(jcrContent != null){
                    InputStream s = (InputStream) jcrContent.getValueMap().get(JcrConstants.JCR_DATA);
                    if(s != null){
                        BufferedReader br;
                        String line;
                        try {
                            br = new BufferedReader(new InputStreamReader(s));
                            while ((line = br.readLine()) != null) {
                                swearsMap.put(line, "");
                            }
                            br.close();
                            s.close();
                            resolver.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    public boolean checkUserNameAgainstFilters(String username) {
        if(StringUtils.isBlank(username) || swearsMap.containsKey(username))
            return false;
        for(String swear : swearsMap.keySet()){
            if(StringUtils.containsIgnoreCase(username, swear))
                return false;
        }
        return true;
    }


}
