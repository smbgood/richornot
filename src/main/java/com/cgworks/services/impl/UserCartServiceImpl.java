package com.cgworks.services.impl;

import com.cgworks.data.RONCart;
import com.cgworks.services.UserCartService;
import com.cgworks.utils.RONUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.resource.*;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;


@Component(immediate = true)
@Service
public class UserCartServiceImpl implements UserCartService{

    private HashMap<String, RONCart> cartMap;

    @Reference
    ResourceResolverFactory rrf;

    @Reference
    SlingRepository repo;

    @Activate
    public void activate(){
        cartMap = new HashMap<String, RONCart>();
        //need to check our persistent cart storage location for server reboots
        try {
            ResourceResolver resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
            Resource cartCache = resolver.getResource("/etc/carts");
            if(cartCache != null){
                String json = cartCache.getValueMap().get("json", "");
                if(StringUtils.isNotBlank(json)){
                    Type mapType = new TypeToken<HashMap<String, RONCart>>() {} .getType();
                    cartMap = new Gson().fromJson(json, mapType);
                }
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Deactivate
    public void deactivate(){
        //need to write out current cart contents to json so we can reinstantiate in activate
        ResourceResolver resolver = null;
        try {
            resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
            Resource cartCache = resolver.getResource("/etc/carts");
            ModifiableValueMap mvm = cartCache.adaptTo(ModifiableValueMap.class);
            if(cartMap.size() > 0){
                Type mapType = new TypeToken<HashMap<String, RONCart>>() {} .getType();
                String json = new Gson().toJson(cartMap, mapType);
                if(StringUtils.isNotBlank(json))
                    mvm.put("json", json);
            }else
                mvm.put("json", "");

            resolver.commit();
            if(resolver.isLive())
                resolver.close();
        } catch (RepositoryException e) {
            e.printStackTrace();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
    }

    public Map<String, RONCart> getActiveCarts() {
        return cartMap;
    }

    public RONCart getUserCart(String token) {
        return cartMap.containsKey(token) ? cartMap.get(token) : null;
    }

    public void updateCart(String token, RONCart cart) {
        cartMap.put(token, cart);
    }

    public void removeCart(String token) {
        cartMap.remove(token);
    }
}
