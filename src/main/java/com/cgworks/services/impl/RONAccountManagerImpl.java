package com.cgworks.services.impl;

import com.cgworks.data.RONAccount;
import com.cgworks.services.RONAccountManager;
import com.cgworks.utils.RONUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import javax.jcr.SimpleCredentials;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

@Component(immediate = true)
@Service
public class RONAccountManagerImpl implements RONAccountManager {

    private HashMap<String, RONAccount> userHashMap;

    private HashMap<String, RONAccount> userPathHashMap;

    @Reference
    private SlingRepository repo;

    @Reference
    private ResourceResolverFactory rrf;

    public RONAccount getAccountFromToken(String token) {
        if(MapUtils.isNotEmpty(userHashMap) && userHashMap.containsKey(token)){
            return userHashMap.get(token);
        }else{
            return null;
        }
    }

    public RONAccount getAccountFromPath(String path){
        if(MapUtils.isNotEmpty(userPathHashMap) && userPathHashMap.containsKey(path)){
            return userPathHashMap.get(path);
        }
        return null;
    }

    @Activate
    public void activate(){
        try {
            userHashMap = new HashMap<String, RONAccount>();
            userPathHashMap = new HashMap<String, RONAccount>();
            final JackrabbitSession s = (JackrabbitSession) repo.login(new SimpleCredentials("admin", "admin".toCharArray()));
            final UserManager um = s.getUserManager();
            final ResourceResolver resolver = RONUtils.loginWithAdminCredentials(s, rrf);
            Iterator<Authorizable> iter = um.findAuthorizables("jcr:primaryType", "rep:User");
            while(iter.hasNext()){
                Authorizable a = iter.next();
                RONAccount account = new RONAccount(a, resolver);
                if(a != null && a.getID() != null && !a.getID().equals("anonymous") && !a.getPath().contains("system") && !a.getID().equals("admin")){
                    userHashMap.put(account.getUserPrincipal().getSeedHex(), account);
                    userPathHashMap.put(a.getPath(), account);
                }else if(a != null && a.getID() != null && a.getID().equals("admin")){
                    account.setIsAdmin(true);
                    userHashMap.put(account.getUserPrincipal().getSeedHex(), account);
                    userPathHashMap.put(a.getPath(), account);
                }
            }
            s.logout();

        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    public void addAccount(String principalName, RONAccount account){
        userHashMap.put(principalName, account);
        userPathHashMap.put(account.getUserPath(), account);
    }

    public void removeAccount(String token) {
        if(userHashMap != null && userHashMap.containsKey(token)){
            userHashMap.remove(token);
        }
        if(userPathHashMap != null && userPathHashMap.containsKey(token)){
            userPathHashMap.remove(token);
        }
    }

    public List<RONAccount> getUserList() {
        ArrayList<RONAccount> accounts = new ArrayList<RONAccount>();
        accounts.addAll(userHashMap.values());
        return accounts;
    }
}
