package com.cgworks.services.impl;

import com.cgworks.slingmodels.RONComment;
import com.cgworks.services.CommentService;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.*;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.query.Query;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@Component
@Service
public class CommentServiceImpl implements CommentService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Reference
    private
    ResourceResolverFactory rrf;

    @Reference
    private
    SlingRepository repo;

    //useful if you need to check why a sling model won't adapt with mf.createModel methods [or somesuch]
    /*@Reference
    ModelFactory mf;*/

    private HashMap<String, HashMap<String, RONComment>> pathMappings;

    public HashMap<String, HashMap<String, RONComment>> getPathMappings(){
        return pathMappings;
    }

    /*
        In a perfect world, this method would take the new leaf and traverse the comment.replies
        structure until it found where it needed to go, then replace that and every map up the chain
        with the updated map.

        In this one, it takes the updated parent and updates the path mappings for the top level comment,
        which rebuilds the entire tree when it is re-adapted into a resource
     */
    public void updateCommentsForPath(RONComment comment, ResourceResolver resolver){
        if(StringUtils.isNotBlank(comment.getPagePath())){
            if(pathMappings != null){
                if(pathMappings.containsKey(comment.getPagePath())){
                    HashMap<String, RONComment> comments = pathMappings.get(comment.getPagePath());
                    comments.put(comment.getUuid(), comment);
                    pathMappings.put(comment.getPagePath(), comments);
                    log.info("Updated existing comments");
                }else{
                    HashMap<String, RONComment> commentMap = new HashMap<String, RONComment>();
                    commentMap.put(comment.getUuid(), comment);
                    pathMappings.put(comment.getPagePath(), commentMap);
                    log.info("Updated non-existing comments");
                }
            }
        }
    }

    private String getUuidPartFromPath(String path, int ordinal) {
        int beginIndex = StringUtils.ordinalIndexOf(path, "/data/", ordinal) + 6;
        int endIndex = path.indexOf("/", beginIndex) > -1 ? path.indexOf("/", beginIndex) : path.length();
        return path.substring(beginIndex, endIndex);
    }

    @Activate
    public void activate(){

        Session session = RONUtils.loginSessionWithAdmin(repo);

        //UserManager userManager = RONUtils.getUserManagerForSession(session);

        ResourceResolver resolver = RONUtils.loginWithAdminCredentials(session, rrf);
        if(resolver != null){
            //search paths for comments
            //Iterator<Resource> foundResources = resolver.findResources("SELECT * FROM [sling:OrderedFolder] AS s WHERE CONTAINS(s.[sling:resourceType], 'comment-data')", Query.JCR_SQL2);
            Iterator<Resource> foundResources = resolver.findResources("SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE(s,'" + Constants.CONTENT_ROOT +"') AND s.[sling:resourceType]='comment-data'", Query.JCR_SQL2);
            if(!IteratorUtils.isEmpty(foundResources)){
                System.out.println("Beginning Path Mappings Generation for Comment Service");
                pathMappings = new HashMap<String, HashMap<String, RONComment>>();
                int count = 0;
                while(foundResources.hasNext()){
                    Resource r = foundResources.next();
                    if(r != null){
                        if(r.getParent().getParent().isResourceType("comment-data"))
                        {
                            while(!r.getParent().getParent().isResourceType("components/comments")){
                                r = r.getParent().getParent();
                            }
                        }
                        //adapting this will adapt children as well
                        RONComment rc = r.adaptTo(RONComment.class);
                        if(rc != null){
                            //if we are a reply, need to refresh mappings for the parent
                            //rc.setPagePath(r.getPath());
                            count++;
                            if(pathMappings.containsKey(rc.getPagePath())){
                                HashMap<String, RONComment> comments = pathMappings.get(rc.getPagePath());
                                comments.put(rc.getUuid(), rc);
                                pathMappings.put(rc.getPagePath(), comments);
                            }else{
                                HashMap<String, RONComment> commentMap = new HashMap<String, RONComment>();
                                commentMap.put(rc.getUuid(), rc);
                                pathMappings.put(rc.getPagePath(), commentMap);
                            }
                        }
                    }
                }
                System.out.println("Finished Path Mappings with " + pathMappings.size() + " paths. Tried " + count + " comments");
            }
            if(resolver.isLive()){
                resolver.close();
            }
        }
    }
}
