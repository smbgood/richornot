package com.cgworks.services.impl;

import com.cgworks.data.RONNavigation;
import com.cgworks.services.SiteNavService;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;

@Component(immediate=true)
@Service
public class SiteNavServiceImpl implements SiteNavService{

    @Reference
    ResourceResolverFactory rrf;

    @Reference
    SlingRepository repo;

    public RONNavigation siteNavigation;

    @Activate
    public void activate(){
        rebuildSiteNavigationTree();
    }

    public RONNavigation getSiteNavigationInfoForPath(String path) {
        //set our active item before returning object
        RONNavigation nav = siteNavigation;
        nav.setActiveItem(path);
        return nav;
    }

    public void rebuildSiteNavigationTree(){
        try {
            ResourceResolver resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
            Resource contentRoot = resolver.getResource(Constants.CONTENT_ROOT);
            if(contentRoot != null) {
                RONNavigation rootNav = new RONNavigation(contentRoot);
                if(rootNav != null){
                    siteNavigation = rootNav;
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }
}
