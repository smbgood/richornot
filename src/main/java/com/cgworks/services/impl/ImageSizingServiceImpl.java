package com.cgworks.services.impl;

import com.cgworks.services.ImageSizingService;
import com.cgworks.slingmodels.RONImage;
import com.cgworks.slingmodels.RONRendition;
import com.cgworks.utils.RONUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.models.factory.ModelFactory;
import sun.awt.WindowIDProvider;

import javax.jcr.RepositoryException;
import java.util.HashMap;

@Component(immediate = true)
@Service
public class ImageSizingServiceImpl implements ImageSizingService {

    @Reference
    SlingRepository repository;

    @Reference
    ResourceResolverFactory rrf;

    @Reference
    ModelFactory mf;

    //could replace inner hashmap with PhotoSize or some such if we need height
    private HashMap<String, HashMap<Integer, RONRendition>> imageHashMap;

    private final String uploadPath = "/uploads";

    @Activate
    public void activate() throws RepositoryException {
        imageHashMap = new HashMap<String, HashMap<Integer, RONRendition>>();
        ResourceResolver resolver = RONUtils.loginWithAdminCredentials(repository, rrf);
        if(resolver != null && StringUtils.isNotBlank(uploadPath)) {
            Resource r = resolver.getResource(uploadPath);
            if (r.hasChildren()) {
                for(Resource child : r.getChildren()){
                    if(child != null && child.getChild("renditions") != null){
                        RONImage image = mf.createModel(child, RONImage.class);
                        if(image.getRenditions() == null)
                            continue;
                        HashMap<Integer, RONRendition> widthMap = new HashMap<Integer, RONRendition>();
                        for(RONRendition rendition : image.getRenditions()){
                            if(rendition != null && rendition.getImageData() != null && rendition.getImageData().getWidth() != null){
                                widthMap.put(Integer.parseInt(rendition.getImageData().getWidth()), rendition);
                            }
                        }
                        String noExtensionName = image.getFileName().substring(0, image.getFileName().indexOf("."));
                        imageHashMap.put(noExtensionName, widthMap);
                    }
                }

            }
        }
    }

    public RONRendition selectRenditionForNameAndWidth(String name, int width) {
        if(MapUtils.isNotEmpty(imageHashMap) && StringUtils.isNotBlank(name) && imageHashMap.containsKey(name)){
            HashMap<Integer, RONRendition> widthsMap = imageHashMap.get(name);
            if(MapUtils.isNotEmpty(widthsMap)){
                int diff = 0;
                int widthToSelect = -1;
                for(Integer integer : widthsMap.keySet()){
                    if(Math.abs(integer - width) > diff){
                        diff = Math.abs(integer - width);
                        widthToSelect = integer;
                    }
                }
                if(widthToSelect > -1){
                    return widthsMap.get(width);
                }
            }
        }
        return null;
    }
}
