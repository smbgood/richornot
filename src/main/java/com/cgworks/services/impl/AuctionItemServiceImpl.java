package com.cgworks.services.impl;

import com.cgworks.services.AuctionItemService;
import com.cgworks.services.ShopItemService;
import com.cgworks.slingmodels.RONAuctionItem;
import com.cgworks.slingmodels.RONProduct;
import com.cgworks.utils.RONUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component(immediate = true)
@Service
public class AuctionItemServiceImpl implements AuctionItemService {


    @Reference
    ResourceResolverFactory rrf;

    @Reference
    SlingRepository repo;

    private ArrayList<RONAuctionItem> productsForAuction;

    @Override
    public List<RONAuctionItem> getItemsForAuction() {
        return productsForAuction;
    }

    @Activate
    public void activate(){
        try {
            ResourceResolver resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
            if(resolver != null){
                Iterator<Resource> founds = resolver.findResources("SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE(s,'/etc/products/auction') AND s.[sling:resourceType]='components/auction-detail'", Query.JCR_SQL2);
                if(founds.hasNext()){
                    ArrayList<RONAuctionItem> products = new ArrayList<RONAuctionItem>();
                    while(founds.hasNext()){
                        Resource next = founds.next();
                        if(next == null)
                            continue;
                        RONAuctionItem product = next.adaptTo(RONAuctionItem.class);
                        if(product != null){
                            products.add(product);
                        }
                    }
                    productsForAuction = products;
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }


}

