package com.cgworks.services.impl;

import com.cgworks.data.RONCart;
import com.cgworks.services.OrderManager;
import com.cgworks.slingmodels.RONOrder;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@Component(immediate = true)
@Service
public class OrderManagerImpl implements OrderManager {

    @Reference
    private ResourceResolverFactory rrf;

    @Reference
    private SlingRepository repo;

    public String ordersPath = Constants.CONTENT_ROOT + "/orders";

    private HashMap<String, RONOrder> orderMap;

    @Activate
    public void activate(){
        orderMap = new HashMap<String, RONOrder>();

        Session s = RONUtils.loginSessionWithAdmin(repo);

        ResourceResolver resolver = RONUtils.loginWithAdminCredentials(s, rrf);
        if(resolver != null){
            Resource orders = resolver.getResource(ordersPath);
            if(orders != null){
                Iterable<Resource> childs = orders.getChildren();
                for(Resource r : childs){
                    if(r != null){
                        RONOrder order = r.adaptTo(RONOrder.class);
                        if(order != null){
                            orderMap.put(r.getName(), order);
                        }
                    }
                }
            }
        }

    }

    public RONOrder getOrderForId(String orderId) {
        return orderMap.get(orderId);
    }

    public void addOrder(RONOrder order, String name) {
        orderMap.put(name, order);
    }

    public List<RONOrder> getOrders() {
        if(MapUtils.isNotEmpty(orderMap)){
            ArrayList<RONOrder> ronOrders = new ArrayList<RONOrder>();
            ronOrders.addAll(orderMap.values());
            return ronOrders;
        }
        return new ArrayList<RONOrder>();
    }

    public void deleteOrder(String id) {
        if(MapUtils.isNotEmpty(orderMap)){
            if(orderMap.containsKey(id)){
                orderMap.remove(id);
            }
        }
    }

    public List<RONOrder> getOrdersForUserPath(String userPath) {
        ArrayList<RONOrder> returnOrders = new ArrayList<RONOrder>();
        for(RONOrder order : getOrders()){
            if(order != null && StringUtils.isNotBlank(order.getUserOrdered()) && order.getUserOrdered().equals(userPath)){
                returnOrders.add(order);
            }
        }
        return returnOrders;
    }
}
