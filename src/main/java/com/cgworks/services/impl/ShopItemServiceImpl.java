package com.cgworks.services.impl;

import com.cgworks.services.ShopItemService;
import com.cgworks.slingmodels.RONProduct;
import com.cgworks.utils.RONUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.models.factory.ModelFactory;

import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import java.util.*;

@Component(immediate = true)
@Service
public class ShopItemServiceImpl implements ShopItemService {

    @Reference
    ResourceResolverFactory rrf;

    @Reference
    SlingRepository repo;

    private ArrayList<RONProduct> productsForSale;

    public List<RONProduct> getProductsForSale() {
        return productsForSale;
    }

    @Activate
    public void activate(){
        try {
            ResourceResolver resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
            if(resolver != null){
                Iterator<Resource> founds = resolver.findResources("SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE(s,'/etc/products') AND s.[sling:resourceType]='components/shop-detail'", Query.JCR_SQL2);
                if(founds.hasNext()){
                    ArrayList<RONProduct> products = new ArrayList<RONProduct>();
                    while(founds.hasNext()){
                        Resource next = founds.next();
                        if(next == null)
                            continue;
                        RONProduct product = next.adaptTo(RONProduct.class);
                        if(product != null){
                            products.add(product);
                        }
                    }
                    productsForSale = products;
                }
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }
}
