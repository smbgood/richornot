package com.cgworks.services.impl;

import com.cgworks.services.BookingManager;
import com.cgworks.slingmodels.booking.RONMonth;
import com.cgworks.utils.RONUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.resource.*;
import org.apache.sling.jcr.api.SlingRepository;
import org.joda.time.DateTime;

import javax.jcr.RepositoryException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

@Component(immediate=true)
@Service
public class BookingManagerImpl implements BookingManager {

    @Reference
    private SlingRepository repo;

    @Reference
    private ResourceResolverFactory rrf;

    private HashMap<String, Object> resourceProperties = new HashMap<String, Object>() {{
        put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED);
    }};

    @Activate
    public void activate() throws RepositoryException, PersistenceException {
        //build month data tree if not exists
        /*ResourceResolver resolver = RONUtils.loginWithAdminCredentials(repo, rrf);
        if(resolver != null){
            String path = "/etc/bookings";
            Resource calendarRoot = ResourceUtil.getOrCreateResource(resolver, path, resourceProperties, null, true);
            if(calendarRoot != null){
                Date d = new Date();
                DateTime dt = new DateTime(d);

                int year = dt.getYear();
                int month = dt.getMonthOfYear();

                DateTime endTime = dt.plusYears(1);

                //write year, month, day, day, day, etc
                Resource yearRoot = ResourceUtil.getOrCreateResource(resolver, path + "/" + year, resourceProperties, null, true);
                Resource monthRoot = ResourceUtil.getOrCreateResource(resolver, yearRoot.getPath() + "/" + month, resourceProperties, null, true);

                for(DateTime monthInterval = dt; monthInterval.isBefore(endTime); monthInterval = monthInterval.plusMonths(1)) {

                    if (monthInterval.getMonthOfYear() != month){
                        monthInterval = monthInterval.withDayOfMonth(1);
                        if(monthInterval.getYear() != year){
                            year = monthInterval.getYear();
                            yearRoot = ResourceUtil.getOrCreateResource(resolver, path + "/" + year, resourceProperties, null, true);
                        }
                        month = monthInterval.getMonthOfYear();
                        monthRoot = ResourceUtil.getOrCreateResource(resolver, yearRoot.getPath() + "/" + month, resourceProperties, null, true);
                    }
                    for (DateTime interval = monthInterval; interval.isBefore(endTime); interval = interval.plusDays(1)) {
                        //year exceeds old year, move parents

                        //month exceeds old month, move parent
                        if (interval.getMonthOfYear() != month)
                            break;

                        resolver.create(monthRoot, interval.getDayOfMonth() + "", resourceProperties);
                    }
                }

                resolver.commit();
                resolver.close();

            }
        }*/
        //else, parse data which contains upcoming months



    }

    public ArrayList<RONMonth> getMonths() {
        return null;
    }
}
