package com.cgworks.services.impl;

import com.cgworks.services.EmailMessagingService;
import com.cgworks.utils.Constants;
import com.cgworks.utils.RONUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Component
@Service
public class EmailMessagingServiceImpl implements EmailMessagingService{

    private static Session mailSession;

    @Reference
    private ResourceResolverFactory rrf;

    @Reference
    private SlingRepository repo;

    @Activate
    public void activate(){
        Properties mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        mailSession = Session.getDefaultInstance(mailServerProperties, null);
    }

    @Override
    public void sendMessage(final String type, final String subject, final String emailBody, final String sender, final String recipient) {
        Runnable task = () -> {
            Transport transport = null;
            try {
                //switch on type
                String subjectToUse = subject;
                String emailToUse = emailBody;
                switch (type) {
                    case Constants.EMAIL_SIGNUP:
                        emailToUse = "Welcome to richornot";
                        subjectToUse = "Welcome";
                        break;
                    case Constants.EMAIL_ORDER_CREATE:
                        //emailBody is orderJson
                        break;
                }
                //if order needs info added, comes in body param, else, use a constant <html> string for body


                MimeMessage generateMailMessage = new MimeMessage(mailSession);
                generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
                generateMailMessage.addRecipient(Message.RecipientType.BCC, new InternetAddress("tinyspidersquad@gmail.com"));
                generateMailMessage.setSubject(subjectToUse);
                generateMailMessage.setContent(emailToUse, "text/html");

                transport = mailSession.getTransport("smtp");

                ResourceResolver resourceResolver = RONUtils.loginWithAdminCredentials(repo, rrf);
                Resource pwdResource = resourceResolver.getResource(Constants.ZERO_ZERO);
                String password = pwdResource.getValueMap().get("pw", "");

                transport.connect("smtp.gmail.com", "tinyspidersquad@gmail.com", password);
                if (transport.isConnected()) {
                    transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
                }
                transport.close();
            }catch(Exception e) {
                System.out.println(e.getMessage() + e.getCause());
                if (transport != null){
                    try {
                        transport.close();
                    } catch (MessagingException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        };

        Thread emailThread = new Thread(task);
        emailThread.start();
    }

}
