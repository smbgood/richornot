package com.cgworks.services;

import com.cgworks.slingmodels.RONOrder;

import java.util.List;

public interface OrderManager {

    RONOrder getOrderForId(String orderId);

    void addOrder(RONOrder order, String name);

    List<RONOrder> getOrders();

    void deleteOrder(String id);

    List<RONOrder> getOrdersForUserPath(String userPath);
}
