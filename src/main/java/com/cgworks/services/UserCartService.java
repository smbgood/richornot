package com.cgworks.services;

import com.cgworks.data.RONCart;

import java.util.Map;

public interface UserCartService {

    Map<String, RONCart> getActiveCarts();

    RONCart getUserCart(String token);

    void updateCart(String token, RONCart cart);

    void removeCart(String token);

}
