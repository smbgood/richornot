package com.cgworks.services;

import com.cgworks.slingmodels.RONComment;
import org.apache.sling.api.resource.ResourceResolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public interface CommentService {

    void updateCommentsForPath(RONComment comment, ResourceResolver resolver);

    HashMap<String, HashMap<String, RONComment>> getPathMappings();
}
