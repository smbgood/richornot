package com.cgworks.services;

import com.cgworks.data.RONNavigation;

public interface SiteNavService {

    RONNavigation getSiteNavigationInfoForPath(String path);

    void rebuildSiteNavigationTree();

}
