package com.cgworks.services;

public interface UserNameFilterService {

    boolean checkUserNameAgainstFilters(String username);

}
