package com.cgworks.services;

import com.cgworks.slingmodels.RONProduct;

import java.util.List;

public interface ShopItemService {

    List<RONProduct> getProductsForSale();

}
