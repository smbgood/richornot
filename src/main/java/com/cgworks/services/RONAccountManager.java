package com.cgworks.services;


import com.cgworks.data.RONAccount;
import org.apache.jackrabbit.api.security.user.User;

import java.util.List;

public interface RONAccountManager {

    RONAccount getAccountFromToken(String token);

    RONAccount getAccountFromPath(String path);

    void addAccount(String token, RONAccount ronAccount);

    void removeAccount(String token);

    List<RONAccount> getUserList();
}
