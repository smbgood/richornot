package com.cgworks.utils;

import com.cgworks.data.RONAccount;
import com.cgworks.data.RONCart;
import com.cgworks.data.RONItem;
import com.cgworks.services.RONAccountManager;
import com.cgworks.services.UserCartService;
import com.cgworks.services.UserNameFilterService;
import com.cgworks.slingmodels.RONPage;
import com.cgworks.slingmodels.RONProduct;
import com.cgworks.slingmodels.RONUser;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.jcr.api.SlingRepository;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.query.Query;
import javax.script.Bindings;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RONUtils
{
    private static final String largePrime = Integer.toHexString(838052903);

    public static ResourceResolver loginWithAdminCredentials(SlingRepository repo, ResourceResolverFactory rrf) throws RepositoryException {
        Session s = repo.login(new SimpleCredentials("admin", "admin".toCharArray()));
        ResourceResolver resolver = null;
        if(rrf != null){
            try {
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("user.jcr.session", s);
                resolver = rrf.getResourceResolver(params);
            } catch (LoginException e) {
                e.printStackTrace();
            }
        }
        return resolver;
    }

    public static ResourceResolver loginWithAdminCredentials(Session s, ResourceResolverFactory rrf){
        if(rrf != null && s != null){
            try {
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("user.jcr.session", s);
                return rrf.getResourceResolver(params);
            } catch (LoginException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getObfuscatedTokenStringForAlphaString(String tokenIn){
        if(StringUtils.isBlank(tokenIn))
            return "";

        BigInteger result = null;

        //multiple hex by large prime in hex
        try {
            byte[] bytes = Hex.decodeHex(tokenIn.toCharArray());
            byte[] bytes2 = Hex.decodeHex(largePrime.toCharArray());

            BigInteger integer1 = new BigInteger(1, bytes);
            BigInteger integer2 = new BigInteger(1, bytes2);
            result = integer1.multiply(integer2);

        } catch (DecoderException e) {
            e.printStackTrace();
        }

        if(result != null) {
            return Hex.encodeHexString(result.toByteArray());
        }
        return "";
    }

    public static String deObfuscateToken(String token){
        if(StringUtils.isBlank(token))
            return "";
        BigInteger result2 = null;
        try {
            byte[] decoded = Hex.decodeHex(token.toCharArray());
            byte[] amountToDivide = Hex.decodeHex(largePrime.toCharArray());

            BigInteger integer1 = new BigInteger(1, decoded);
            BigInteger integer2 = new BigInteger(1, amountToDivide);

            result2 = integer1.divide(integer2);

            final byte[] obj = result2.toByteArray();
            return RONUtils.asHex(obj);
        } catch (DecoderException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String toHex(String arg) {
        return String.format("%040x", new BigInteger(1, arg.getBytes(Charset.forName("UTF-8"))));
    }

    private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();

    public static String asHex(byte[] buf)
    {
        char[] chars = new char[2 * buf.length];
        for (int i = 0; i < buf.length; ++i)
        {
            chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
            chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
        }
        return new String(chars);
    }

    public static RONCart getOrCreateCartAndUpdate(UserCartService cartService, ResourceResolver resolver, String productPath, int quantity, String userToken, boolean isGuest) {
        if(cartService != null && StringUtils.isNotBlank(userToken)){
            RONCart cart = cartService.getUserCart(userToken);
            if(cart == null)
                cart = new RONCart(userToken, isGuest);
            if(resolver != null)
                cart.addItem(resolver.getResource(productPath), quantity);
            cartService.updateCart(userToken, cart);
            return cart;
        }
        return null;
    }

    public static String safeGetRequestParam(String paramName, SlingHttpServletRequest request) {
        if(StringUtils.isNotBlank(paramName) && request != null && request.getRequestParameterMap().size() > 0){
            RequestParameter p = request.getRequestParameter(paramName);
            if(p != null){
                return p.getString();
            }
        }
        return "";
    }

    public static String safeGetCookie(String cookieName, SlingHttpServletRequest request) {
        if(request != null && StringUtils.isNotBlank(cookieName)){
            Cookie c = request.getCookie(cookieName);
            if(c != null){
                return c.getValue();
            }
        }
        return "";
    }

    public static String safeGetAttribute(String attribute, SlingHttpServletRequest request) {
        if(StringUtils.isNotBlank(attribute)){
            HttpSession session = request.getSession();
            if(session != null){
                return (String) session.getAttribute(attribute);
            }
        }
        return "";
    }

    public static RONPage findPageByResourceType(String resourceType, SlingHttpServletRequest request) {
        ResourceResolver resolver = request.getResourceResolver();
        if(resolver != null) {
            Iterator<Resource> founds = resolver.findResources("SELECT * FROM [sling:OrderedFolder] AS s WHERE ISDESCENDANTNODE(s,'" + Constants.CONTENT_ROOT + "') AND s.[sling:resourceType]='" + resourceType + "'", Query.JCR_SQL2);
            if (founds.hasNext()) {
                return founds.next().adaptTo(RONPage.class);
            }
        }
        return null;
    }

    public static Session loginSessionWithAdmin(SlingRepository repo) {
        Session session = null;
        try {
            session = repo.login(new SimpleCredentials("admin", "admin".toCharArray()));
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        return session;
    }

    public static UserManager getUserManagerForSession(Session session) {
        if(session instanceof JackrabbitSession)
        {
            try {
                UserManager um = ((JackrabbitSession) session).getUserManager();
                if(um != null){
                    return um;
                }
            } catch (RepositoryException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static boolean checkUserNameFilter(String username, UserNameFilterService filterService) {
        if(filterService != null){
            return filterService.checkUserNameAgainstFilters(username);
        }
        return true;
    }

    public static void outputErrorJson(String errorMessage, SlingHttpServletResponse response) {
        if(StringUtils.isNotBlank(errorMessage)){
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json");
            try {
                response.getWriter().write("{\"error\":\"" + errorMessage + "\"}");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static RONAccount getUserAccountForUse(Bindings bindings) {
        final String usertoken = RONUtils.safeGetCookie(Constants.USER_COOKIE_TOKEN_STRING, (SlingHttpServletRequest) bindings.get("request"));
        return getUserAccountForToken(usertoken, (SlingScriptHelper) bindings.get("sling"));

    }

    public static RONAccount getUserAccountForToken(String usertoken, SlingScriptHelper helper){
        String token = RONUtils.deObfuscateToken(usertoken);

        if(StringUtils.isNotBlank(token)) {
            RONAccountManager accountManager = helper.getService(RONAccountManager.class);
            return accountManager.getAccountFromToken(token);
        }
        return null;
    }

    public static String safeGetRequestParamFromMap(String paramName, RequestParameterMap parameterMap) {
        if(StringUtils.isNotBlank(paramName) && parameterMap != null && parameterMap.containsKey(paramName)) {
            RequestParameter[] requestParameters = parameterMap.get(paramName);
            if(requestParameters != null) {
                RequestParameter requestParameter = requestParameters[0];
                String s = requestParameter.getString();
                if(StringUtils.isNotBlank(s))
                    return s;
            }
        }
        return "";
    }

    public static int checkQuantities(RONItem item, ResourceResolver resolver) {
        int orderableQty = 0;

        int qtyWanted = item.getQuantity();

        Resource resource = resolver.getResource(item.getProductPath());
        RONProduct p = resource.adaptTo(RONProduct.class);
        if(p != null){
            int qtyAvailable = Integer.parseInt(p.getQuantityAvailable());
            if(qtyAvailable <= 0)
                return orderableQty;
            int updatedQuantity = qtyAvailable - qtyWanted;
            if(updatedQuantity > -1){
                orderableQty = qtyWanted;
            }else{
                int amountToRemove = Math.abs(qtyAvailable - qtyWanted);
                orderableQty = qtyWanted - amountToRemove;
            }
        }

        return orderableQty;
    }

    /**
     * scale image
     *
     * @param sbi image to scale
     * @param imageType type of image
     * @param dWidth width of destination image
     * @param dHeight height of destination image
     * @param fWidth x-factor for transformation / scaling
     * @param fHeight y-factor for transformation / scaling
     * @return scaled image
     */
    public static BufferedImage scale(BufferedImage sbi, int imageType, int dWidth, int dHeight, double fWidth, double fHeight) {
        BufferedImage dbi = null;
        if(sbi != null) {
            dbi = new BufferedImage(dWidth, dHeight, imageType);
            Graphics2D g = dbi.createGraphics();
            AffineTransform at = AffineTransform.getScaleInstance(fWidth, fHeight);
            g.drawRenderedImage(sbi, at);
        }
        return dbi;
    }
}
