package com.cgworks.utils;

import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;

public class Incrementer implements Use {

    private Long currentValue;

    public void init(Bindings bindings) {
        currentValue = (Long) bindings.get("value");
    }

    public Long getNextValue(){
        currentValue++;

        return currentValue;
    }
}
