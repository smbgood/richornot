package com.cgworks.utils;

public class Constants {

    public static final String CONTENT_ROOT = "/content/en";
    public static final String USER_COOKIE_TOKEN_STRING = "x-morbis-flng";
    public static final String DEFAULT_REDIRECT_HOME = CONTENT_ROOT + "/home";
    public static final String HTML_SUFFIX = ".html";

    public static final String GUEST_COOKIE_TOKEN_STRING = "guestToken";

    public static final String ORDER_RTYPE = "components/order";
    public static final String CHECKOUT_RTYPE = "components/checkout";
    public static final String USERNAME_FILTER_FILE_PATH = "/etc/other/swears.txt";
    public static final String REVIEW_RTYPE = "components/order-review";
    public static final String EMAIL_SIGNUP = "account_created";
    public static final String ADMIN_FROM_EMAIL = "tinyspidersquad@gmail.com";
    public static final String EMAIL_ORDER_CREATE = "order_created";
    public static final String ZERO_ZERO = "/etc/00";
}
