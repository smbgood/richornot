$(function(){

    var grandTotal = $('.grand-total-amt');
    var amt = grandTotal.text();
    grandTotal.text(d3.format('$4.2f')(amt));

    var priceFields = $('.item-price > .item-price-amount');
    $.each(priceFields, function(index, value){
        var priceParent = $(priceFields[index]).parent().parent();
        var pricePer = priceParent.find(":input[name='item-price-per']").val();
        var quantity = priceParent.find(':input[name="previous-qty"]').val();
        var number = parseFloat(pricePer * quantity);
        $(priceFields[index]).text(d3.format('$4.2f')(number));
    });
    var updateGrandTotal = function () {
        var total = 0.0;
        $.each(priceFields, function(index, value){
            var dollarString = $(priceFields[index]).text();
            dollarString = dollarString.replace("$", "");
            total += parseFloat(dollarString);
        });
        grandTotal.text(d3.format('$4.2f')(total));
    };
    $('#order-body').on('change', ':input[type="number"]', null, function(thing){
        var changedInput = $(thing.target);
        changedInput.parent().find('.alert-warning').hide();
        var productToCheck = changedInput.attr("data-path");
        var quantity = changedInput.val();
        var parent = changedInput.parent().parent();
        var priceField = parent.find('.item-price-amount');

        //post to servlet
        $.post('/bin/cart/quantity', { itemPath: productToCheck, quantity: quantity}, function(data){
            var amount = d3.format("$4.2f")(data.priceTotal);
            if(data.isAllowed){
                priceField.text(amount);
                parent.find(':input[name="previous-qty"]').val(quantity);
            }else{
                changedInput.val(parent.find(':input[name="previous-qty"]').val());
                changedInput.parent().find('.alert-warning').show();
            }
            updateGrandTotal();
        });

    });
});
