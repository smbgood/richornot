$('.ron-signup').on('click', function(){
    var nameError = $('#name-error');
    var pwdError = $('#pwd-error');
    var emailError = $('#email-error');
    var nameExists = $('#name-exists');
    var pwdShort = $('#pwd-short');
    var pwdMatch = $('#pwd-match');
    var emailInput = $('#email-input');
    var signUpError = $('#ron-signup-error');
    var nameBad = $('#name-bad');
    var emailInUse = $('#email-in-use');

    nameError.hide();
    pwdError.hide();
    emailError.hide();
    pwdShort.hide();
    pwdMatch.hide();
    emailInput.hide();
    signUpError.hide();
    nameExists.hide();
    nameBad.hide();
    emailInUse.hide();

    var username = $('#ron-username').val();
    var password = $('#ron-pwd').val();
    var passwordConfirm = $('#ron-pwd-valid').val();
    var email = $('#ron-email').val();
    if(!username){
        nameError.show();
    }
    if(!password || !passwordConfirm){
        pwdError.show();
    }else{
        if(password.length < 4){
            pwdShort.show();
            return;
        }
        if(password != passwordConfirm) {
            pwdMatch.show();
            return;
        }
    }
    if(!email){
        emailError.show();
    }else{
        if(email.indexOf("@") == -1 || email.indexOf(".") == -1){
            emailInput.show();
            return;
        }
    }
    if(!username || !password || !email){
        return;
    }
    $.post('/bin/account/create', {username : username, password: password, email:email}).done(
        function(data){
            //set cookie to page, refresh page to trigger updated contents
            if(data.token){
                createCookie("x-morbis-flng", data.token, 30);
                window.location.replace(window.location.pathname+"?");
            }else if(data.error){
                var message = data.error;
                if(message.indexOf("Authorizable") > -1 && message.indexOf("already exists") > -1){
                    nameExists.show();
                    return;
                }else if(message === "swears"){
                    nameBad.show();
                    return;
                }else if(message === "email in use"){
                    emailInUse.show();
                    return;
                }
                signUpError.show();
                console.error(data.error);
            }
        }
    );
});
$('.ron-login').on('click', function(){
    var userName = $('#ron-username-login').val();
    var password = $('#ron-pwd-login').val();
    var nameError = $('#ron-name-error');
    var passwordError = $('#ron-pwd-error');
    nameError.hide();
    passwordError.hide();
    if(!userName)
        nameError.show();
    if(!password)
        passwordError.show();
    if(!userName || ! password)
        return;
    $.post('/bin/account/login', {username : userName, password: password}).done(
        function(data){
            //set cookie to page, refresh page to trigger updated contents
            if(data.token){
                createCookie("x-morbis-flng", data.token, 30);
                window.location.replace(window.location.pathname +"?");
            }else if(data.error){
                $('#ron-login-error').show();
                console.error(data.error);
            }
        }
    );

});
$('.ron-logout').on('click', function(){
    $.post('/bin/account/logout').done(
        function(data){
            eraseCookie("x-morbis-flng");
            window.location.replace(window.location.pathname+"?");
        }
    );
});
$('.ron-signup-toggle').on('click', function(){
    $('#ron-login-parent').hide();
    $('#ron-signup-parent').show();
});
$('.ron-login-toggle').on('click', function(){
    $('#ron-signup-parent').hide();
    $('#ron-login-parent').show();
});