package com.cgworks.slingmodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class RONBid {

    @Inject
    @Optional
    private double bidAmount;

    @Inject
    @Optional
    private String bidUser;

    @Inject
    @Optional
    private String bidUserPath;

    public double getBidAmount() {
        return bidAmount;
    }

    public String getBidUser() {
        return bidUser;
    }

    public String getBidUserPath() {
        return bidUserPath;
    }
}
