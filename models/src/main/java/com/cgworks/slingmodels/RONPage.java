package com.cgworks.slingmodels;


import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.UUID;

@Model(adaptables=Resource.class)
public class RONPage {

    public String resourcePath;

    @Inject @Optional
    @Named("sling:resourceType")
    public String resourceType;

    @Inject @Optional
    public String title;

    @Inject @Optional
    public String navTitle;

    @Inject @Optional
    public String requiresAuth;

    @Inject @Optional
    public String hideFromNav;

    public String getUuid() {
        return uuid;
    }

    private String uuid;

    public RONPage(Resource resource) {
        this.resource = resource;
        this.uuid = RandomStringUtils.randomAlphabetic(10);
    }

    private transient final Resource resource;

    @PostConstruct
    public void init(){
        resourcePath = resource.getPath();
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public String getResourceType() {
        return resourceType;
    }

    public String getTitle() {
        return title;
    }

    public String getNavTitle() {
        return navTitle;
    }

    public String getRequiresAuth() {
        return requiresAuth;
    }

    public boolean getHideFromNav() {
        return BooleanUtils.isTrue(Boolean.valueOf(hideFromNav));
    }

    public void setHideFromNav(String hideFromNav) {
        this.hideFromNav = hideFromNav;
    }

}
