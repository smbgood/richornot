package com.cgworks.slingmodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;
import javax.inject.Named;

@Model(adaptables=Resource.class)
public class TestData {
    public String getSomeProperty() {
        return someProperty;
    }

    @Inject @Optional
    @Named("someProp")
    private String someProperty;

}
