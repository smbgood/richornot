package com.cgworks.slingmodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables=Resource.class)
public class RONImage {

    @Inject @Optional @Named("renditions")
    private List<RONRendition> renditions;

    private final Resource resource;

    private String fileName;

    public RONImage(Resource resource) {
        this.resource = resource;
    }

    @PostConstruct
    public void init(){
        if(this.resource != null)
            this.fileName = this.resource.getName();
    }

    public String getFileName() {
        return fileName;
    }

    public List<RONRendition> getRenditions() {
        return renditions;
    }

}
