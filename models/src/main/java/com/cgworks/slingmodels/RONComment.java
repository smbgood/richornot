package com.cgworks.slingmodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Model(adaptables=Resource.class)
public class RONComment {

    @Inject @Optional
    @Named("message")
    private String someMessage;

    @Inject @Optional
    @Named("author")
    private String attribution;

    @Inject @Named("data") @Optional
    private List<RONComment> replies;

    private HashMap<String, RONComment> replyMap;

    private int nodeDepth;

    private String nodePath;

    private String pagePath;

    private String uuid;

    private String uuidPath;

    public void setPagePath(String path){
        if(path.indexOf("/") > -1){
            //store uuid
            uuid = path.substring(path.lastIndexOf("/") +1, path.length());
            uuidPath = path.substring(path.indexOf("/data"), path.length());
            int depth = 0;
            while(path.contains("data")) {
                depth++;
                path = path.substring(0, path.lastIndexOf("/data"));
            }
            pagePath = path.substring(0, path.length());
            nodeDepth = depth;
        }
    }

    public RONComment(Resource resource) {
        this.resource = resource;
    }

    private final Resource resource;

    public String getAttribution() {
        return attribution;
    }

    public String getPagePath() {
        return pagePath;
    }

    public int getNodeDepth() {
        return nodeDepth;
    }

    public void setNodePath(String path) {
        nodePath = path;
    }

    public String getNodePath() {
        return nodePath;
    }

    public List<RONComment> getReplies() {
        return replies;
    }

    public void setReplies(List<RONComment> replies) {
        this.replies = replies;
        setupReplyMap();
    }

    public String getSomeMessage() {
        return someMessage;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String inId){
        uuid = inId;
    }

    public String getUuidPath() {
        return uuidPath;
    }

    @PostConstruct
    public void setThings(){
        setNodePath(resource.getPath());
        setPagePath(resource.getPath());
        setupReplyMap();
    }

    private void setupReplyMap() {
        if(replyMap == null)
            replyMap = new HashMap<String, RONComment>();
        if(replies != null){
            for(RONComment reply : replies){
                replyMap.put(reply.getUuid(), reply);
            }
        }
    }

    public HashMap<String, RONComment> getReplyMap() {
        return replyMap;
    }

    public void setReplyMap(HashMap<String, RONComment> replyMap) {
        this.replyMap = replyMap;
        this.replies = (List<RONComment>) replyMap.values();
    }

}
