package com.cgworks.slingmodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class RONUser {

    @Inject @Optional
    private String email;

    public String getEmail() {
        return email;
    }

    public String getSeed() {
        return seed;
    }

    @Inject @Optional
    private String seed;
}
