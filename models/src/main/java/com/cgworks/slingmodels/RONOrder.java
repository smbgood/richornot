package com.cgworks.slingmodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Model(adaptables=Resource.class)
public class RONOrder {

    public String getTimeString() {
        return timeString;
    }

    private String timeString = "";

    @Inject @Optional
    private String orderStatus;

    @Inject @Optional
    private String orderTotal;

    @Inject @Optional
    private String guestEmail;

    @Inject @Optional
    private String guestName;

    @Inject @Optional
    private String guestAddress;

    @Inject @Optional
    private String orderJson;

    @Inject @Optional
    private boolean isGuestOrder;

    @Inject @Optional
    private String userOrdered;

    @Inject @Optional
    private Long createdDate;

    private GregorianCalendar calendar;

    public Long getCreatedDate(){ return createdDate; }

    public String getUserOrdered(){
        return userOrdered;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public String getGuestEmail() {
        return guestEmail;
    }

    public String getGuestName() {
        return guestName;
    }

    public String getGuestAddress() {
        return guestAddress;
    }

    public String getOrderJson() {
        return orderJson;
    }

    public boolean isGuestOrder() {
        return isGuestOrder;
    }

    public RONOrder(Resource resource) {
        this.resourcePath = resource != null ? resource.getPath() : "";
    }

    @PostConstruct
    public void init(){
        if(createdDate > 0){
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTimeInMillis(createdDate);
            timeString = new SimpleDateFormat("dd MMM yyyy hh:mm aa zzz").format(cal.getTime());
        }
    }

    public String getResourcePath() {
        return resourcePath;
    }

    private final String resourcePath;

}
