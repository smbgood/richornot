package com.cgworks.slingmodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

@Model(adaptables = Resource.class)
public class RONRendition {

    @Inject
    @Named("jcr:content") @Optional
    private RONImageData imageData;

    private String url;
    private String fileName;
    private final Resource resource;

    public RONRendition(Resource resource) {
        this.resource = resource;
    }

    @PostConstruct
    public void init(){
        if(resource != null)
            this.fileName = resource.getName();
        if(resource != null)
            this.url = resource.getPath();
    }

    public String getFileName() {
        return fileName;
    }


    public RONImageData getImageData() {
        return imageData;
    }

    public String getUrl() {
        return url;
    }

}
