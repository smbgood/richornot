package com.cgworks.slingmodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Model(adaptables=Resource.class)
public class RONProduct implements Serializable {

    @Inject
    @Optional
    public String title = "";

    @Inject
    @Optional
    public String thumbnailUrl = "";

    @Inject
    @Optional
    public String detailLink = "";

    @Inject
    @Optional
    public String price = "";

    @Inject
    @Optional
    public String category = "";

    @Inject
    @Optional
    private String[] imageNames;

    public RONProduct() {
        this.resource = null;
    }

    public String getTitle() {
        return title;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getDetailLink() {
        return detailLink;
    }

    public String getPrice() {
        return price;
    }

    public String getQuantityAvailable() {
        return quantityAvailable;
    }

    public String getCategory() {
        return category;
    }

    public String[] getImageNames() {
        return imageNames;
    }

    @Inject
    @Optional @Named("quantity")
    public String quantityAvailable;

    private String resourcePath;

    public RONProduct(Resource resource) {
        this.resource = resource;
    }

    private transient final Resource resource;

    @PostConstruct
    public void init(){
        resourcePath = resource.getPath();
    }

    public String getResourcePath() {
        return resourcePath;
    }

}
