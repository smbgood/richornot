package com.cgworks.slingmodels;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.InputStream;

@Model(adaptables=Resource.class)
public class RONImageData {

    public String getMimeType() {
        return mimeType;
    }

    @Inject
    @Named("jcr:mimeType")
    private String mimeType;

    @Inject @Named("ron:width") @Optional
    private String width;

    @Inject @Named("ron:height") @Optional
    private String height;

    private transient final Resource resource;

    private InputStream stream;

    public RONImageData(Resource resource) {
        this.resource = resource;
    }

    @PostConstruct
    public void init()
    {
        stream = resource.getValueMap().get("jcr:data", InputStream.class);
    }

    public InputStream getStream() {
        return stream;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }
}
