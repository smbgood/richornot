package com.cgworks.slingmodels;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Model(adaptables=Resource.class)
public class RONAuctionItem extends RONProduct{

    public String getCurrentBidPath() {
        return currentBidPath;
    }

    @Inject
    @Optional
    private String currentBidPath;

    private RONBid currentBid;

    @Inject @Named("bids") @Optional
    private List<RONBid> bidList;

    private String resourcePath;

    public RONAuctionItem(Resource resource) {
        this.resource = resource;
        //get child 'bids' resource, adapt to List<Bid>

    }

    private transient final Resource resource;

    @PostConstruct
    public void init(){
        resourcePath = resource.getPath();
        if(resource.getChild("bids/" + currentBidPath) != null)
            currentBid = resource.getChild("bids/" + currentBidPath).adaptTo(RONBid.class);
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public RONBid getCurrentBid() {
        return currentBid;
    }
}
