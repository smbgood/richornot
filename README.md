The richornot ecommerce project was written over the course of 3-4 months as an 
exercise in writing an AEM-like CMS to run an eshop using Open Source software.
To this end, in getting started, the Sling Launchpad project was of great use. 
Once I had a acquired a similar stack to AEM/CQ: a Java back-end, HTL/Sightly for
templating on the front end, with the robustness of Sling to handle complex requests
and the hierarchy of the JCR to keep everything organized, I had an environment
that was pretty close to what I had been using for big businesses just a few years
prior. I tried to incorporated many of the time-savers I'd learned about at those places
while designing my webapp. One that we used heavily was the Sling Models package,
which allows you to pull a JCR node structure quickly into a Java data structure,
allowing for manipulations along the way. Having to add the package myself, this time,
I had to learn some key lessons along the way about how to organize a Maven build
so that everything would be installed/activated correctly in the build process.
Unfortunately, the project never made its way into production, but that fact alone
is what allows me today to provide the code for sampling purposes.